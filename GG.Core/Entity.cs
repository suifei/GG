﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace GG.Core
{
    #region User
    /// <summary>
    /// 用户实体。
    /// </summary>
    [Serializable]
    public class User
    {
        public User() { }
        public User(string id, string pwd, string _name, string _signature, int headIndex)
        {
            this.iD = id;
            this.passwordMD5 = pwd;
            this.name = _name;
            this.signature = _signature;
            this.headImageIndex = headIndex;
        }

        #region ID
        private string iD = "";
        /// <summary>
        /// 用户登录帐号。
        /// </summary>
        public string ID
        {
            get { return iD; }
            set { iD = value; }
        }
        #endregion

        #region PasswordMD5
        private string passwordMD5 = "";
        /// <summary>
        /// 登录密码(MD5加密)。
        /// </summary>
        public string PasswordMD5
        {
            get { return passwordMD5; }
            set { passwordMD5 = value; }
        }
        #endregion

        #region Name
        private string name = "";
        /// <summary>
        /// 昵称
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        #endregion

        #region Signature
        private string signature = "";
        /// <summary>
        /// 签名
        /// </summary>
        public string Signature
        {
            get { return signature; }
            set { signature = value; }
        }
        #endregion

        #region HeadImageIndex
        private int headImageIndex = 0;
        /// <summary>
        /// 头像图片的索引
        /// </summary>
        public int HeadImageIndex
        {
            get { return headImageIndex; }
            set { headImageIndex = value; }
        }
        #endregion

        #region HeadImage
        private byte[] headImage = null;
        /// <summary>
        /// 如果使用自拍头像，则headImage不为null，且headImageIndex为-1。 V3.5
        /// </summary>
        public byte[] HeadImage
        {
            get { return headImage; }
            set { headImage = value; }
        } 
        #endregion

        #region Online
        private bool online = false;
        /// <summary>
        /// 是否在线
        /// </summary>
        public bool Online
        {
            get { return online; }
            set { online = value; }
        }
        #endregion
    } 
    #endregion

    #region Group
    [Serializable]
    public class Group
    {
        public Group() { }
        public Group(string id, string _name, List<string> _members)
        {
            this.iD = id;
            this.name = _name;
            this.members = _members;
        }

        #region ID
        private string iD = "";
        public string ID
        {
            get { return iD; }
            set { iD = value; }
        }
        #endregion

        #region Name
        private string name = "";
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        #endregion

        #region Members
        private List<string> members = new List<string>();
        public List<string> Members
        {
            get { return members; }
            set { members = value; }
        }
        #endregion
    } 
    #endregion

    #region OfflineMessage
    /// <summary>
    /// 离线消息。
    /// </summary>
    [Serializable]
    public class OfflineMessage
    {
        #region Ctor
        public OfflineMessage() { }
        public OfflineMessage(string _sourceUserID, string _destUserID, int _informationType, byte[] info)
        {
            this.sourceUserID = _sourceUserID;
            this.destUserID = _destUserID;
            this.informationType = _informationType;
            this.information = info;
        }
        #endregion

        #region SourceUserID
        private string sourceUserID = "";
        /// <summary>
        /// 发送离线消息的用户ID。
        /// </summary>
        public string SourceUserID
        {
            get { return sourceUserID; }
            set { sourceUserID = value; }
        }
        #endregion

        #region DestUserID
        private string destUserID = "";
        /// <summary>
        /// 接收离线消息的用户ID。
        /// </summary>
        public string DestUserID
        {
            get { return destUserID; }
            set { destUserID = value; }
        }
        #endregion

        #region InformationType
        private int informationType = 0;
        /// <summary>
        /// 信息的类型。
        /// </summary>
        public int InformationType
        {
            get { return informationType; }
            set { informationType = value; }
        }
        #endregion

        #region Information
        private byte[] information;
        /// <summary>
        /// 信息内容
        /// </summary>
        public byte[] Information
        {
            get { return information; }
            set { information = value; }
        }
        #endregion

        #region Time
        private DateTime time = DateTime.Now;
        /// <summary>
        /// 服务器接收到要转发离线消息的时间。
        /// </summary>
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
        #endregion
    }
    #endregion

    #region OfflineFileItem
    /// <summary>
    /// 离线文件条目
    /// </summary>
    public class OfflineFileItem
    {        
        /// <summary>
        /// 条目的唯一编号，数据库自增序列，主键。
        /// </summary>
        public string AutoID { get; set; }
       
        /// <summary>
        /// 离线文件的名称。
        /// </summary>
        public string FileName { get; set; }
       
        /// <summary>
        /// 文件的大小。
        /// </summary>
        public ulong FileLength { get; set; }
       
        /// <summary>
        /// 发送者ID。
        /// </summary>
        public string SenderID { get; set; }
     
        /// <summary>
        /// 接收者ID。
        /// </summary>
        public string AccepterID { get; set; }
       
        /// <summary>
        /// 在服务器上存储离线文件的临时路径。
        /// </summary>
        public string RelayFilePath { get; set; }
    }
    #endregion

    /// <summary>
    /// 针对InformationTypes.OfflineFileResultNotify的消息协议
    /// </summary>
    public class OfflineFileResultNotifyContract
    {
        public OfflineFileResultNotifyContract() { }
        public OfflineFileResultNotifyContract(string accepterID, string file, bool accept)
        {
            this.AccepterID = accepterID;
            this.FileName = file;
            this.Accept = accept;
        }

        public string AccepterID { get; set; }

        public string FileName { get; set; }

        public bool Accept { get; set; }
    }

    public class CreateGroupContract
    {
        public CreateGroupContract() { }
        public CreateGroupContract(string id,string name)
        {
            this.ID = id;
            this.Name = name;
        }

        public string ID { get; set; }

        public string Name { get; set; }
    }

    public class ChangePasswordContract
    {
        public ChangePasswordContract() { }
        public ChangePasswordContract(string oldPasswordMD5, string newPasswordMD5)
        {
            this.OldPasswordMD5 = oldPasswordMD5;
            this.NewPasswordMD5 = newPasswordMD5;
        }

        public string OldPasswordMD5 { get; set; }

        public string NewPasswordMD5 { get; set; }
    }
}