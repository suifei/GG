using System;
using System.IO ;
using System.Runtime.Serialization ;
using System.Runtime.Serialization.Formatters.Binary ;
using System.Xml.Serialization;
using System.Text;
using Microsoft.Win32;

namespace GG.Core
{
	
	public static class GGHelper
	{      
        #region SerializeObject
        public static byte[] SerializeObject(object obj) //obj 可以是数组
        {
            IFormatter formatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream();//此种情况下,mem_stream的缓冲区大小是可变的

            formatter.Serialize(memoryStream, obj);

            byte[] buff = memoryStream.ToArray();
            memoryStream.Close();

            return buff;
        }

        public static void SerializeObject(object obj, ref byte[] buff, int offset) //obj 可以是数组
        {
            byte[] rude_buff = GGHelper.SerializeObject(obj);
            for (int i = 0; i < rude_buff.Length; i++)
            {
                buff[offset + i] = rude_buff[i];
            }
        }
        #endregion

        #region DeserializeBytes
        public static object DeserializeBytes(byte[] buff, int index, int count)
        {
            IFormatter formatter = new BinaryFormatter();

            MemoryStream stream = new MemoryStream(buff, index, count);
            object obj = formatter.Deserialize(stream);
            stream.Close();

            return obj;
        }
        #endregion         

        #region XmlObject
        public static string XmlObject(object obj)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
            MemoryStream stream = new MemoryStream();
            xmlSerializer.Serialize(stream, obj);
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            string res = reader.ReadToEnd();
            stream.Close();

            return res;
        }
        #endregion

        #region ObjectXml
        public static T ObjectXml<T>(string str)
        {
            return (T)GGHelper.ObjectXml(str, typeof(T));
        }

        public static object ObjectXml(string str, Type targetType)
        {
            byte[] buff = System.Text.Encoding.Default.GetBytes(str);
            XmlSerializer xmlSerializer = new XmlSerializer(targetType);
            MemoryStream stream = new MemoryStream(buff, 0, buff.Length);
            object obj = xmlSerializer.Deserialize(stream);
            stream.Close();

            return obj;
        }
        #endregion

        #region GenerateFile
        /// <summary>
        /// 将字符串写成文件
        /// </summary>       
        public static void GenerateFile(string filePath, string text)
        {
            string directoryPath = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);

            StreamWriter sw = new StreamWriter(fs);

            sw.Write(text);
            sw.Flush();

            sw.Close();
            fs.Close();
        }
        #endregion

        #region GetFileContent
        /// <summary>
        /// 读取文本文件的内容
        /// </summary>       
        public static string GetFileContent(string file_path)
        {
            if (!File.Exists(file_path))
            {
                return null;
            }

            StreamReader reader = new StreamReader(file_path, Encoding.UTF8);
            string content = reader.ReadToEnd();
            reader.Close();

            return content;
        }
        #endregion

        #region 开机自动启动
        /// <summary> 
        /// 开机自动启动,使用注册表 
        /// </summary> 
        /// <param name=\"Started\">是否开机自动启动</param> 
        /// <param name=\"name\">取一个唯一的注册表Key名称</param> 
        /// <param name=\"path\">启动程序的完整路径</param> 
        public static void RunWhenStart(bool started, string name, string path)
        {
            RegistryKey HKLM = Registry.LocalMachine;
            try
            {
                RegistryKey run = HKLM.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\");
                if (started)
                {
                    run.SetValue(name, path);
                }
                else
                {
                    run.DeleteValue(name);
                }
            }
            finally
            {
                HKLM.Close();
            }
        }
        #endregion      
    }	
}
