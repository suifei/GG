﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GG.Core
{
    public interface IGGPersister
    {
        void AddFriend(string ownerID, string friendID);
        void RemoveFriend(string ownerID, string friendID);
        List<string> GetFriends(string userID);

        CreateGroupResult CreateGroup(string creatorID, string groupID, string groupName);
        JoinGroupResult JoinGroup(string userID, string groupID);
        void QuitGroup(string userID, string groupID);
        Group GetGroup(string groupID);
        List<string> GetGroupmates(string userID);
        List<Group> GetMyGroups(string userID);

        User GetUser(string userID);
        void InsertUser(User user);
        bool IsUserExist(string userID);
        ChangePasswordResult ChangePassword(string userID, string oldPasswordMD5, string newPasswordMD5);

        void StoreOfflineMessage(OfflineMessage msg);
        List<OfflineMessage> PickupOfflineMessage(string destUserID);

        void StoreOfflineFileItem(OfflineFileItem item);
        List<OfflineFileItem> PickupOfflineFileItem(string accepterID);
    }
}
