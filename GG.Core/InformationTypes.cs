﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GG.Core
{
    /// <summary>
    /// 自定义信息的类型，取值0-100
    /// </summary>
    public static class InformationTypes
    {
        /// <summary>
        /// 聊天信息 0
        /// </summary>
        public const int Chat = 0;

        /// <summary>
        /// 窗口抖动 1
        /// </summary>
        public const int Vibration = 1;

        /// <summary>
        /// 请求视频 2
        /// </summary>
        public const int VideoRequest = 2;

        /// <summary>
        /// 同意视频 3 （C->C）
        /// </summary>
        public const int AgreeVideo = 3;

        /// <summary>
        /// 拒绝视频 4 （C->C）
        /// </summary>
        public const int RejectVideo = 4;

        /// <summary>
        /// 挂断视频 5 （C->C）
        /// </summary>
        public const int HungUpVideo = 5;


        /// <summary>
        /// 请求访问对方的磁盘 6 （C->C）
        /// </summary>
        public const int DiskRequest = 6;

        /// <summary>
        /// 同意磁盘访问 7 （C->C）
        /// </summary>
        public const int AgreeDisk = 7;

        /// <summary>
        /// 拒绝磁盘访问 8 （C->C）
        /// </summary>
        public const int RejectDisk = 8;

        /// <summary>
        /// 请求对方远程协助自己（访问自己的桌面） 9 （C->C）
        /// </summary>
        public const int RemoteHelpRequest = 9;

        /// <summary>
        /// 同意远程协助对方 10 （C->C）
        /// </summary>
        public const int AgreeRemoteHelp = 10;

        /// <summary>
        /// 拒绝远程协助对方 （C->C）
        /// </summary>
        public const int RejectRemoteHelp = 11;

        /// <summary>
        /// 请求方终止了协助 （C->C）
        /// </summary>
        public const int TerminateRemoteHelp = 12;

        /// <summary>
        /// 协助方终止了协助 （C->C）
        /// </summary>
        public const int CloseRemoteHelp = 13;

        /// <summary>
        /// 请求离线消息 （C->S）
        /// </summary>
        public const int GetOfflineMessage = 14;

        /// <summary>
        /// 服务端转发离线消息给客户端 （S->C）
        /// </summary>
        public const int OfflineMessage = 15;

        /// <summary>
        /// 请求离线文件 （C->S）
        /// </summary>
        public const int GetOfflineFile = 16;

        /// <summary>
        /// 通知发送方，对方是接收了离线文件，还是拒绝了离线文件。（S->C）
        /// </summary>
        public const int OfflineFileResultNotify = 17;


        /// <summary>
        /// 语音留言 （S->C）
        /// </summary>
        public const int OfflineAudioMessage = 18;

        /// <summary>
        /// 修改密码。
        /// </summary>
        public const int ChangePassword = 50;

        /// <summary>
        /// 获取好友列表（C->S）
        /// </summary>
        public const int GetFriends = 51;

        /// <summary>
        /// 获取用户信息（C->S）
        /// </summary>
        public const int GetUserInfo = 52;

        /// <summary>
        /// 添加好友（C->S）
        /// </summary>
        public const int AddFriend = 53;

        /// <summary>
        /// 通知客户端其被对方添加为好友（S->C）
        /// </summary>
        public const int FriendAddedNotify = 54;

        /// <summary>
        /// 获取我的所有组ID（C->S）
        /// </summary>
        public const int GetMyGroups = 55;

        /// <summary>
        /// 加入组（C->S）
        /// </summary>
        public const int JoinGroup = 56;

        /// <summary>
        /// 获取组资料（C->S）
        /// </summary>
        public const int GetGroup = 57;

        /// <summary>
        /// 创建组（C->S）
        /// </summary>
        public const int CreateGroup = 58;

        /// <summary>
        /// 退出组（C->S）
        /// </summary>
        public const int QuitGroup = 59;


        /// <summary>
        /// 删除好友（C->S）
        /// </summary>
        public const int RemoveFriend = 60;

        /// <summary>
        /// 通知客户端其被对方从好友中删除（S->C）
        /// </summary>
        public const int FriendRemovedNotify = 61;

        //取值0-100。 V1.9
        public static bool ContainsInformationType(int infoType)
        {
            return infoType >= 0 && infoType <= 100;
        }
    }


    public static class BroadcastTypes
    {
        /// <summary>
        /// 群聊天
        /// </summary>
        public const int BroadcastChat = 0;

        /// <summary>
        /// 某用户加入组
        /// </summary>
        public const int SomeoneJoinGroup = 1;

        /// <summary>
        /// 某用户退出组
        /// </summary>
        public const int SomeoneQuitGroup = 2;
    }
}
