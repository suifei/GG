﻿using System;
using System.Collections.Generic;
using System.Text;
using GG.Core;

namespace GG.Server
{
    internal class FriendsManager : ESPlus.Application.Friends.Server.IFriendsManager
    {
        private IGGPersister ggPersister;
        public FriendsManager(IGGPersister db)
        {
            this.ggPersister = db;
        }

        public List<string> GetFriendsList(string ownerID, string tag)
        {
            return this.ggPersister.GetFriends(ownerID);
        }
    }
}
