﻿using System;
using System.Collections.Generic;
using System.Text;
using GG.Core;

namespace GG.Server
{
    internal class GroupManager : ESPlus.Application.Group.Server.IGroupManager
    {
        private IGGPersister ggPersister;
        public GroupManager(IGGPersister db)
        {
            this.ggPersister = db;
        }

        public List<string> GetGroupMembers(string groupID)
        {
            Group group =  this.ggPersister.GetGroup(groupID);
            if (group == null)
            {
                return new List<string>();
            }

            return group.Members;
        }

        public List<string> GetGroupmates(string userID)
        {
            return this.ggPersister.GetGroupmates(userID);
        }
    }
}
