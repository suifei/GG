﻿using System;
using System.Collections.Generic;
using System.Text;
using ESPlus.Rapid;
using GG.Core;
using ESPlus.Serialization;
using OMCS.Server;

namespace GG.Server
{
    /// <summary>
    /// 离线消息控制器 V3.2
    /// 最近更新 V3.6 以增加对 语音留言 的支持
    /// </summary>
    internal class OfflineMessageController
    {
        private IRapidServerEngine rapidServerEngine;
        private IGGPersister ggPersister;
        private IMultimediaServer multimediaServer;

        public OfflineMessageController(IRapidServerEngine engine ,IGGPersister db ,IMultimediaServer mServer )
        {
            this.rapidServerEngine = engine;
            this.ggPersister = db;
            this.multimediaServer = mServer;
            this.rapidServerEngine.CustomizeController.TransmitFailed += new ESBasic.CbGeneric<ESPlus.Application.Information>(CustomizeController_TransmitFailed);
            this.multimediaServer.MicroMessagePostFailed += new ESBasic.CbGeneric<string, string, OMCS.Passive.MicroMessages.MicroMessage>(multimediaServer_MicroMessagePostFailed);
          }

        //V3.6 离线语音消息 - 语音留言
        void multimediaServer_MicroMessagePostFailed(string accepterID, string sourceID, OMCS.Passive.MicroMessages.MicroMessage microMessage)
        {
            byte[] content = CompactPropertySerializer.Default.Serialize(microMessage);
            OfflineMessage msg = new OfflineMessage(sourceID, accepterID, InformationTypes.OfflineAudioMessage, content);
            this.ggPersister.StoreOfflineMessage(msg);
            return;
        }        

        //消息转发失败
        void CustomizeController_TransmitFailed(ESPlus.Application.Information info)
        {
            if (info.InformationType == InformationTypes.Chat) //目前只保存离线的聊天消息
            {
                OfflineMessage msg = new OfflineMessage(info.SourceID, info.DestID, info.InformationType, info.Content);
                this.ggPersister.StoreOfflineMessage(msg);
                return;
            }
        }

        //发送离线消息给客户端
        public void SendOfflineMessage(string destUserID)
        {            
            List<OfflineMessage> list = this.ggPersister.PickupOfflineMessage(destUserID);
            if (list != null && list.Count > 0)
            {
                foreach (OfflineMessage msg in list)
                {
                    byte[] bMsg = CompactPropertySerializer.Default.Serialize<OfflineMessage>(msg);
                    this.rapidServerEngine.CustomizeController.Send(msg.DestUserID, InformationTypes.OfflineMessage, bMsg);
                }
            }

        }
    }
}
