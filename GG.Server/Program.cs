﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ESPlus.Widgets;
using ESPlus.Rapid;
using ESPlus.Application.CustomizeInfo.Server;
using ESFramework;
using ESFramework.Server.UserManagement;
using ESPlus.Core;
using ESPlus.Application.Friends.Server;
using System.Configuration;
using OMCS;
using OMCS.Server;
using ESPlus.Application.CustomizeInfo;
using GGLib.NDisk.Server;
using System.Runtime.Remoting;
using GG.Core;
/*
 * 本demo采用的是ESFramework的免费版本，不需要再次授权、也没有使用期限限制。若想获取ESFramework其它版本，请联系 www.oraycn.com 或 QQ：372841921。
 * 
 */
namespace GG.Server
{
    static class Program
    {
        private static IRapidServerEngine RapidServerEngine = RapidEngineFactory.CreateServerEngine();
        private static IMultimediaServer MultimediaServer;
        private static IGGPersister GGPersister = new VirtualDB();      

        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                               
             
                #region 初始化ESFramework服务端引擎   
                ESPlus.GlobalUtil.SetAuthorizedUser("FreeUser", "");               
                ESPlus.GlobalUtil.SetMaxLengthOfUserID(20);
                //自定义的好友管理器
                FriendsManager friendManager = new FriendsManager(Program.GGPersister);
                Program.RapidServerEngine.FriendsManager = friendManager;
                //自定义的组管理器
                GroupManager groupManager = new GroupManager(Program.GGPersister);
                Program.RapidServerEngine.GroupManager = groupManager;


                NDiskHandler nDiskHandler = new NDiskHandler(); //网盘处理器 V1.9
                CustomizeHandler handler = new CustomizeHandler();
                ComplexCustomizeHandler complexHandler = new ComplexCustomizeHandler(nDiskHandler, handler); 

                //初始化服务端引擎
                Program.RapidServerEngine.SecurityLogEnabled = false;
                Program.RapidServerEngine.Initialize(int.Parse(ConfigurationManager.AppSettings["Port"]), complexHandler, new BasicHandler(Program.GGPersister));
               

                //初始化网盘处理器 V1.9
                NetworkDiskPathManager networkDiskPathManager = new GGLib.NDisk.Server.NetworkDiskPathManager() ;
                NetworkDisk networkDisk = new NetworkDisk(networkDiskPathManager, Program.RapidServerEngine.FileController);
                nDiskHandler.Initialize(Program.RapidServerEngine.FileController, networkDisk);

                //设置重登陆模式
                Program.RapidServerEngine.UserManager.RelogonMode = RelogonMode.ReplaceOld; 
               
                //离线文件控制器 V3.2
                OfflineFileController offlineFileController = new OfflineFileController(Program.RapidServerEngine, Program.GGPersister);                
                #endregion            

                #region 初始化OMCS服务器
                OMCS.GlobalUtil.SetAuthorizedUser("FreeUser", "");
                OMCS.GlobalUtil.SetMaxLengthOfUserID(20);
                OMCSConfiguration config = new OMCSConfiguration(
                    int.Parse(ConfigurationManager.AppSettings["CameraFramerate"]),
                    int.Parse(ConfigurationManager.AppSettings["DesktopFramerate"]),
                    (EncodingQuality)Enum.Parse(typeof(EncodingQuality), ConfigurationManager.AppSettings["AudioQuality"]),
                    int.Parse(ConfigurationManager.AppSettings["WaveSampleRate"]),
                    int.Parse(ConfigurationManager.AppSettings["WhiteboardWidth"]),
                    int.Parse(ConfigurationManager.AppSettings["WhiteboardHeight"]));

                //用于验证登录用户的帐密
                DefaultUserVerifier userVerifier = new DefaultUserVerifier();
                Program.MultimediaServer = MultimediaServerFactory.CreateMultimediaServer(int.Parse(ConfigurationManager.AppSettings["OmcsPort"]), userVerifier, config,false);                           
                
                #endregion


                //离线消息控制器 V3.2
                OfflineMessageController offlineMessageController = new OfflineMessageController(Program.RapidServerEngine, Program.GGPersister, Program.MultimediaServer);
                handler.Initialize(Program.GGPersister, Program.RapidServerEngine, offlineMessageController, offlineFileController);

                #region 发布用于注册的Remoting服务
                RemotingConfiguration.Configure("GG.Server.exe.config", false);
                RegisterService registerService = new Server.RegisterService(Program.GGPersister);
                RemotingServices.Marshal(registerService, "RegisterService");      
                #endregion       
 
                //如果不需要默认的UI显示，可以替换下面这句为自己的Form
                ESPlus.Widgets.MainServerForm mainForm = new ESPlus.Widgets.MainServerForm(Program.RapidServerEngine);
                mainForm.Text = "GG2013 服务器";
                Application.Run(mainForm);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }       
    }
}
