﻿using System;
using System.Collections.Generic;
using System.Text;
using GG.Core;

namespace GG.Server
{
    internal class RegisterService :MarshalByRefObject, IRegisterService
    {
        private IGGPersister ggPersister;
        public RegisterService(IGGPersister db)
        {
            this.ggPersister = db;
        }

        public RegisterResult Register(User user)
        {
            try
            {
                if (this.ggPersister.IsUserExist(user.ID))
                {
                    return RegisterResult.Existed;
                }

                this.ggPersister.InsertUser(user);
                return RegisterResult.Succeed;
            }
            catch (Exception ee)
            {
                return RegisterResult.Error;
            }
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}
