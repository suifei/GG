﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CCWin;
using CCWin.Win32;
using CCWin.Win32.Const;
using System.Diagnostics;
using GG.Core;
using System.Configuration;
using ESPlus.Rapid;

namespace GG
{
    public partial class AddFriendForm : CCSkinMain
    {
        private int headImageIndex = 0;
        private IRapidPassiveEngine rapidPassiveEngine;

        public AddFriendForm(IRapidPassiveEngine engine)
        {
            InitializeComponent();
            this.rapidPassiveEngine = engine;
        }

        #region FriendID
        private string friendID = "";
        public string FriendID
        {
            get
            {
                return this.friendID;
            }
        } 
        #endregion        

        private void skinButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.friendID = this.skinTextBox_id.SkinTxt.Text.Trim();
            if (this.friendID.Length == 0)
            {
                MessageBox.Show("帐号不能为空！");
                return;
            }

            try
            {
                byte[] bRes = this.rapidPassiveEngine.CustomizeOutter.Query(InformationTypes.AddFriend, System.Text.Encoding.UTF8.GetBytes(this.friendID));
                AddFriendResult res = (AddFriendResult)BitConverter.ToInt32(bRes,0);
                if (res == AddFriendResult.FriendNotExist)
                {
                    MessageBox.Show("帐号不存在！");
                    return;
                }

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception ee)
            {
                MessageBox.Show("添加好友失败！" + ee.Message);
            }
        }      
         
    }
}
