﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ESBasic;
using OMCS.Passive;
using OMCS.Passive.MicroMessages;

namespace GG
{
    /// <summary>
    /// 语音消息面板
    /// </summary>
    public partial class AudioMessagePanel : UserControl
    {     
        private IMultimediaManager multimediaManager;
        private string friendID;

        /// <summary>
        /// 语音消息完成。
        /// </summary>
        public event CbGeneric<MicroMessage> AudioMessageFinished;     

        public AudioMessagePanel()
        {
            InitializeComponent();
            this.Disposed += new EventHandler(AudioMessagePanel_Disposed);         
        }

        void AudioMessagePanel_Disposed(object sender, EventArgs e)
        {
            if (this.multimediaManager != null)
            {
                this.multimediaManager.MicroMessageController.Dispose();
            }
        }

        public void Initialize(IMultimediaManager mgr ,string friend)
        {
            this.multimediaManager = mgr;
            this.friendID = friend;
            this.multimediaManager.MicroMessageController.MicroMessageMode = OMCS.Passive.MicroMessages.MicroMessageMode.JustAudio;
            this.multimediaManager.MicroMessageController.Initialize();
        }
        
        public void Reset()
        {
            this.multimediaManager = null;
            this.friendID = null;
            this.skinButton2.Text = "请按住说话";
            this.skinButton1.Visible = true;
        }

        private void skinButtomReject_Click(object sender, EventArgs e)
        {
            if (this.AudioMessageFinished != null)
            {
                this.AudioMessageFinished(null);
            }                        
        }       
        
        private void OnTerminate()
        {
            this.timerLabel1.Visible = false;
            this.timerLabel1.Stop();
        }        

        private void skinButton2_MouseDown(object sender, MouseEventArgs e)
        {
            this.skinButton1.Visible = false;
            this.timerLabel1.Visible = true;
            this.timerLabel1.Reset();
            this.timerLabel1.Start();
            this.skinButton2.Text = "松开结束";
            this.multimediaManager.MicroMessageController.StartCapture();
        }

        private void skinButton2_MouseUp(object sender, MouseEventArgs e)
        {
            this.OnTerminate();
            MicroMessage microMessage = this.multimediaManager.MicroMessageController.StopCapture();
            this.multimediaManager.MicroMessageController.Send(microMessage, this.friendID);
            this.multimediaManager.MicroMessageController.Dispose();
            this.Reset();

            if (this.AudioMessageFinished != null)
            {
                this.AudioMessageFinished(microMessage);
            }
        }
        
    }
}
