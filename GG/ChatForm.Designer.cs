﻿namespace GG
{
    partial class ChatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChatForm));
            this.labelFriendName = new CCWin.SkinControl.SkinLabel();
            this.labelFriendSignature = new CCWin.SkinControl.SkinLabel();
            this.pnlTx = new CCWin.SkinControl.SkinPanel();
            this.panelFriendHeadImage = new CCWin.SkinControl.SkinPanel();
            this.toolShow = new System.Windows.Forms.ToolTip(this.components);
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.imgyy2 = new System.Windows.Forms.PictureBox();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.skinPanel_right = new CCWin.SkinControl.SkinPanel();
            this.skinTabControl1 = new CCWin.SkinControl.SkinTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.imgMeShow = new System.Windows.Forms.PictureBox();
            this.imgChatShow = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.liklblgg = new System.Windows.Forms.LinkLabel();
            this.skinButtom3 = new CCWin.SkinControl.SkinButton();
            this.btnSend = new CCWin.SkinControl.SkinButton();
            this.btnClose = new CCWin.SkinControl.SkinButton();
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.skinPanel_left = new CCWin.SkinControl.SkinPanel();
            this.textBoxSend = new CCWin.SkinControl.RtfRichTextBox();
            this.rtfRichTextBox_history = new CCWin.SkinControl.RtfRichTextBox();
            this.skToolMenu = new CCWin.SkinControl.SkinToolStrip();
            this.toolFont = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonEmotion = new System.Windows.Forms.ToolStripButton();
            this.toolVibration = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton4 = new System.Windows.Forms.ToolStripSplitButton();
            this.显示消息记录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.显示比例ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.放大Ctrl滚轮ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.缩小ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.清屏ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.消息管理器ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.skinToolStrip2 = new CCWin.SkinControl.SkinToolStrip();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.ToolFile = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem33 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem34 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSplitButton2 = new System.Windows.Forms.ToolStripSplitButton();
            this.请求远程协助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.桌面共享指定区域ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.skinLabel_p2PState = new CCWin.SkinControl.SkinLabel();
            this.pictureBox_state = new System.Windows.Forms.PictureBox();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.pnlTx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgyy2)).BeginInit();
            this.skinPanel_right.SuspendLayout();
            this.skinTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgMeShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgChatShow)).BeginInit();
            this.skinPanel1.SuspendLayout();
            this.skinPanel_left.SuspendLayout();
            this.skToolMenu.SuspendLayout();
            this.skinToolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_state)).BeginInit();
            this.SuspendLayout();
            // 
            // labelFriendName
            // 
            this.labelFriendName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFriendName.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.Anamorphosis;
            this.labelFriendName.AutoSize = true;
            this.labelFriendName.BackColor = System.Drawing.Color.Transparent;
            this.labelFriendName.BorderColor = System.Drawing.Color.White;
            this.labelFriendName.BorderSize = 4;
            this.labelFriendName.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelFriendName.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.labelFriendName.ForeColor = System.Drawing.Color.Black;
            this.labelFriendName.Location = new System.Drawing.Point(55, 7);
            this.labelFriendName.Name = "labelFriendName";
            this.labelFriendName.Size = new System.Drawing.Size(88, 25);
            this.labelFriendName.TabIndex = 100;
            this.labelFriendName.Text = "我的好友";
            this.labelFriendName.MouseEnter += new System.EventHandler(this.labelFriendName_MouseEnter);
            this.labelFriendName.MouseLeave += new System.EventHandler(this.labelFriendName_MouseLeave);
            // 
            // labelFriendSignature
            // 
            this.labelFriendSignature.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFriendSignature.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.Anamorphosis;
            this.labelFriendSignature.AutoSize = true;
            this.labelFriendSignature.BackColor = System.Drawing.Color.Transparent;
            this.labelFriendSignature.BorderColor = System.Drawing.Color.White;
            this.labelFriendSignature.BorderSize = 4;
            this.labelFriendSignature.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelFriendSignature.ForeColor = System.Drawing.Color.Black;
            this.labelFriendSignature.Location = new System.Drawing.Point(80, 32);
            this.labelFriendSignature.Name = "labelFriendSignature";
            this.labelFriendSignature.Size = new System.Drawing.Size(80, 17);
            this.labelFriendSignature.TabIndex = 103;
            this.labelFriendSignature.Text = "我的个性签名";
            // 
            // pnlTx
            // 
            this.pnlTx.BackColor = System.Drawing.Color.Transparent;
            this.pnlTx.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTx.BackRectangle = new System.Drawing.Rectangle(5, 5, 5, 5);
            this.pnlTx.Controls.Add(this.panelFriendHeadImage);
            this.pnlTx.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.pnlTx.DownBack = global::GG.Properties.Resources.aio_head_normal;
            this.pnlTx.Location = new System.Drawing.Point(6, 7);
            this.pnlTx.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTx.MouseBack = global::GG.Properties.Resources.aio_head_normal;
            this.pnlTx.Name = "pnlTx";
            this.pnlTx.NormlBack = global::GG.Properties.Resources.aio_head_normal;
            this.pnlTx.Palace = true;
            this.pnlTx.Size = new System.Drawing.Size(46, 46);
            this.pnlTx.TabIndex = 104;
            // 
            // panelFriendHeadImage
            // 
            this.panelFriendHeadImage.BackColor = System.Drawing.Color.Transparent;
            this.panelFriendHeadImage.BackgroundImage = global::GG.Properties.Resources._1_100;
            this.panelFriendHeadImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelFriendHeadImage.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.panelFriendHeadImage.DownBack = null;
            this.panelFriendHeadImage.Location = new System.Drawing.Point(3, 3);
            this.panelFriendHeadImage.Margin = new System.Windows.Forms.Padding(0);
            this.panelFriendHeadImage.MouseBack = null;
            this.panelFriendHeadImage.Name = "panelFriendHeadImage";
            this.panelFriendHeadImage.NormlBack = null;
            this.panelFriendHeadImage.Radius = 5;
            this.panelFriendHeadImage.Size = new System.Drawing.Size(40, 40);
            this.panelFriendHeadImage.TabIndex = 6;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.AutoSize = false;
            this.toolStripLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLabel2.Image = global::GG.Properties.Resources.pictureBox1_Image;
            this.toolStripLabel2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripLabel2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(10, 24);
            this.toolStripLabel2.Text = "toolStripButton6";
            // 
            // imgyy2
            // 
            this.imgyy2.BackColor = System.Drawing.Color.Transparent;
            this.imgyy2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgyy2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgyy2.Image = global::GG.Properties.Resources.icon_weibo;
            this.imgyy2.Location = new System.Drawing.Point(60, 32);
            this.imgyy2.Margin = new System.Windows.Forms.Padding(0);
            this.imgyy2.Name = "imgyy2";
            this.imgyy2.Size = new System.Drawing.Size(16, 16);
            this.imgyy2.TabIndex = 123;
            this.imgyy2.TabStop = false;
            // 
            // fontDialog1
            // 
            this.fontDialog1.Color = System.Drawing.SystemColors.ControlText;
            this.fontDialog1.ShowColor = true;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "1.gif");
            this.imageList1.Images.SetKeyName(1, "2.gif");
            this.imageList1.Images.SetKeyName(2, "3.gif");
            this.imageList1.Images.SetKeyName(3, "4.gif");
            this.imageList1.Images.SetKeyName(4, "5.gif");
            this.imageList1.Images.SetKeyName(5, "6.gif");
            this.imageList1.Images.SetKeyName(6, "7.gif");
            this.imageList1.Images.SetKeyName(7, "8.gif");
            this.imageList1.Images.SetKeyName(8, "9.gif");
            this.imageList1.Images.SetKeyName(9, "10.gif");
            this.imageList1.Images.SetKeyName(10, "11.gif");
            this.imageList1.Images.SetKeyName(11, "12.gif");
            this.imageList1.Images.SetKeyName(12, "13.gif");
            this.imageList1.Images.SetKeyName(13, "14.gif");
            this.imageList1.Images.SetKeyName(14, "15.gif");
            this.imageList1.Images.SetKeyName(15, "16.gif");
            this.imageList1.Images.SetKeyName(16, "17.gif");
            this.imageList1.Images.SetKeyName(17, "18.gif");
            this.imageList1.Images.SetKeyName(18, "19.gif");
            this.imageList1.Images.SetKeyName(19, "20.gif");
            this.imageList1.Images.SetKeyName(20, "21.gif");
            this.imageList1.Images.SetKeyName(21, "22.gif");
            this.imageList1.Images.SetKeyName(22, "23.gif");
            this.imageList1.Images.SetKeyName(23, "24.gif");
            this.imageList1.Images.SetKeyName(24, "25.gif");
            this.imageList1.Images.SetKeyName(25, "26.gif");
            this.imageList1.Images.SetKeyName(26, "27.gif");
            this.imageList1.Images.SetKeyName(27, "28.gif");
            this.imageList1.Images.SetKeyName(28, "29.gif");
            this.imageList1.Images.SetKeyName(29, "30.gif");
            this.imageList1.Images.SetKeyName(30, "31.gif");
            this.imageList1.Images.SetKeyName(31, "32.gif");
            this.imageList1.Images.SetKeyName(32, "33.gif");
            this.imageList1.Images.SetKeyName(33, "34.gif");
            this.imageList1.Images.SetKeyName(34, "35.gif");
            this.imageList1.Images.SetKeyName(35, "36.gif");
            this.imageList1.Images.SetKeyName(36, "37.gif");
            this.imageList1.Images.SetKeyName(37, "38.gif");
            this.imageList1.Images.SetKeyName(38, "39.gif");
            this.imageList1.Images.SetKeyName(39, "40.gif");
            this.imageList1.Images.SetKeyName(40, "41.gif");
            this.imageList1.Images.SetKeyName(41, "42.gif");
            this.imageList1.Images.SetKeyName(42, "43.gif");
            this.imageList1.Images.SetKeyName(43, "44.gif");
            this.imageList1.Images.SetKeyName(44, "45.gif");
            this.imageList1.Images.SetKeyName(45, "46.gif");
            this.imageList1.Images.SetKeyName(46, "47.gif");
            this.imageList1.Images.SetKeyName(47, "48.gif");
            this.imageList1.Images.SetKeyName(48, "49.gif");
            this.imageList1.Images.SetKeyName(49, "50.gif");
            this.imageList1.Images.SetKeyName(50, "51.gif");
            this.imageList1.Images.SetKeyName(51, "52.gif");
            this.imageList1.Images.SetKeyName(52, "53.gif");
            this.imageList1.Images.SetKeyName(53, "54.gif");
            this.imageList1.Images.SetKeyName(54, "55.gif");
            this.imageList1.Images.SetKeyName(55, "56.gif");
            this.imageList1.Images.SetKeyName(56, "57.gif");
            this.imageList1.Images.SetKeyName(57, "58.gif");
            this.imageList1.Images.SetKeyName(58, "59.gif");
            this.imageList1.Images.SetKeyName(59, "60.gif");
            this.imageList1.Images.SetKeyName(60, "61.gif");
            this.imageList1.Images.SetKeyName(61, "62.gif");
            this.imageList1.Images.SetKeyName(62, "63.gif");
            this.imageList1.Images.SetKeyName(63, "64.gif");
            this.imageList1.Images.SetKeyName(64, "65.gif");
            this.imageList1.Images.SetKeyName(65, "66.gif");
            this.imageList1.Images.SetKeyName(66, "67.gif");
            this.imageList1.Images.SetKeyName(67, "68.gif");
            this.imageList1.Images.SetKeyName(68, "69.gif");
            this.imageList1.Images.SetKeyName(69, "70.gif");
            this.imageList1.Images.SetKeyName(70, "71.gif");
            this.imageList1.Images.SetKeyName(71, "72.gif");
            this.imageList1.Images.SetKeyName(72, "73.gif");
            this.imageList1.Images.SetKeyName(73, "74.gif");
            this.imageList1.Images.SetKeyName(74, "75.gif");
            this.imageList1.Images.SetKeyName(75, "76.gif");
            this.imageList1.Images.SetKeyName(76, "77.gif");
            this.imageList1.Images.SetKeyName(77, "78.gif");
            this.imageList1.Images.SetKeyName(78, "79.gif");
            this.imageList1.Images.SetKeyName(79, "80.gif");
            this.imageList1.Images.SetKeyName(80, "81.gif");
            this.imageList1.Images.SetKeyName(81, "82.gif");
            this.imageList1.Images.SetKeyName(82, "83.gif");
            this.imageList1.Images.SetKeyName(83, "84.gif");
            this.imageList1.Images.SetKeyName(84, "85.gif");
            this.imageList1.Images.SetKeyName(85, "86.gif");
            this.imageList1.Images.SetKeyName(86, "87.gif");
            this.imageList1.Images.SetKeyName(87, "88.gif");
            this.imageList1.Images.SetKeyName(88, "89.gif");
            this.imageList1.Images.SetKeyName(89, "90.gif");
            this.imageList1.Images.SetKeyName(90, "91.gif");
            this.imageList1.Images.SetKeyName(91, "92.gif");
            this.imageList1.Images.SetKeyName(92, "93.gif");
            this.imageList1.Images.SetKeyName(93, "94.gif");
            this.imageList1.Images.SetKeyName(94, "95.gif");
            this.imageList1.Images.SetKeyName(95, "96.gif");
            this.imageList1.Images.SetKeyName(96, "97.gif");
            this.imageList1.Images.SetKeyName(97, "98.gif");
            this.imageList1.Images.SetKeyName(98, "99.gif");
            this.imageList1.Images.SetKeyName(99, "100.gif");
            this.imageList1.Images.SetKeyName(100, "101.gif");
            this.imageList1.Images.SetKeyName(101, "102.gif");
            this.imageList1.Images.SetKeyName(102, "103.gif");
            this.imageList1.Images.SetKeyName(103, "104.gif");
            this.imageList1.Images.SetKeyName(104, "105.gif");
            this.imageList1.Images.SetKeyName(105, "106.gif");
            this.imageList1.Images.SetKeyName(106, "107.gif");
            this.imageList1.Images.SetKeyName(107, "108.gif");
            this.imageList1.Images.SetKeyName(108, "109.gif");
            this.imageList1.Images.SetKeyName(109, "110.gif");
            this.imageList1.Images.SetKeyName(110, "111.gif");
            this.imageList1.Images.SetKeyName(111, "112.gif");
            this.imageList1.Images.SetKeyName(112, "113.gif");
            this.imageList1.Images.SetKeyName(113, "114.gif");
            this.imageList1.Images.SetKeyName(114, "115.gif");
            this.imageList1.Images.SetKeyName(115, "116.gif");
            this.imageList1.Images.SetKeyName(116, "117.gif");
            this.imageList1.Images.SetKeyName(117, "118.gif");
            this.imageList1.Images.SetKeyName(118, "119.gif");
            this.imageList1.Images.SetKeyName(119, "120.gif");
            this.imageList1.Images.SetKeyName(120, "121.gif");
            this.imageList1.Images.SetKeyName(121, "122.gif");
            this.imageList1.Images.SetKeyName(122, "123.gif");
            this.imageList1.Images.SetKeyName(123, "124.gif");
            this.imageList1.Images.SetKeyName(124, "125.gif");
            this.imageList1.Images.SetKeyName(125, "126.gif");
            this.imageList1.Images.SetKeyName(126, "127.gif");
            this.imageList1.Images.SetKeyName(127, "128.gif");
            this.imageList1.Images.SetKeyName(128, "129.gif");
            this.imageList1.Images.SetKeyName(129, "130.gif");
            this.imageList1.Images.SetKeyName(130, "131.gif");
            this.imageList1.Images.SetKeyName(131, "132.gif");
            this.imageList1.Images.SetKeyName(132, "133.gif");
            this.imageList1.Images.SetKeyName(133, "134.gif");
            this.imageList1.Images.SetKeyName(134, "135.gif");
            // 
            // skinPanel_right
            // 
            this.skinPanel_right.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel_right.Controls.Add(this.skinTabControl1);
            this.skinPanel_right.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel_right.Dock = System.Windows.Forms.DockStyle.Right;
            this.skinPanel_right.DownBack = null;
            this.skinPanel_right.Location = new System.Drawing.Point(426, 28);
            this.skinPanel_right.MouseBack = null;
            this.skinPanel_right.Name = "skinPanel_right";
            this.skinPanel_right.NormlBack = null;
            this.skinPanel_right.Size = new System.Drawing.Size(200, 458);
            this.skinPanel_right.TabIndex = 124;
            // 
            // skinTabControl1
            // 
            this.skinTabControl1.BaseColor = System.Drawing.Color.White;
            this.skinTabControl1.BorderColor = System.Drawing.Color.White;
            this.skinTabControl1.BtnArrowDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.BtnArrowDown")));
            this.skinTabControl1.BtnArrowHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.BtnArrowHover")));
            this.skinTabControl1.BtnArrowNorml = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.BtnArrowNorml")));
            this.skinTabControl1.Controls.Add(this.tabPage1);
            this.skinTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabControl1.ItemSize = new System.Drawing.Size(70, 36);
            this.skinTabControl1.Location = new System.Drawing.Point(0, 0);
            this.skinTabControl1.Name = "skinTabControl1";
            this.skinTabControl1.PageColor = System.Drawing.Color.White;
            this.skinTabControl1.PageDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageDown")));
            this.skinTabControl1.PageHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageHover")));
            this.skinTabControl1.PageNorml = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageNorml")));
            this.skinTabControl1.SelectedIndex = 0;
            this.skinTabControl1.Size = new System.Drawing.Size(200, 458);
            this.skinTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.skinTabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.imgMeShow);
            this.tabPage1.Controls.Add(this.imgChatShow);
            this.tabPage1.Location = new System.Drawing.Point(4, 40);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(192, 414);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "头像";
            // 
            // imgMeShow
            // 
            this.imgMeShow.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.imgMeShow.BackColor = System.Drawing.Color.Transparent;
            this.imgMeShow.Image = ((System.Drawing.Image)(resources.GetObject("imgMeShow.Image")));
            this.imgMeShow.Location = new System.Drawing.Point(27, 272);
            this.imgMeShow.Name = "imgMeShow";
            this.imgMeShow.Size = new System.Drawing.Size(140, 140);
            this.imgMeShow.TabIndex = 126;
            this.imgMeShow.TabStop = false;
            // 
            // imgChatShow
            // 
            this.imgChatShow.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imgChatShow.BackColor = System.Drawing.Color.Transparent;
            this.imgChatShow.Image = ((System.Drawing.Image)(resources.GetObject("imgChatShow.Image")));
            this.imgChatShow.Location = new System.Drawing.Point(25, 0);
            this.imgChatShow.Name = "imgChatShow";
            this.imgChatShow.Size = new System.Drawing.Size(140, 226);
            this.imgChatShow.TabIndex = 125;
            this.imgChatShow.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(423, 28);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 458);
            this.splitter1.TabIndex = 125;
            this.splitter1.TabStop = false;
            this.splitter1.Visible = false;
            // 
            // liklblgg
            // 
            this.liklblgg.ActiveLinkColor = System.Drawing.Color.Black;
            this.liklblgg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.liklblgg.AutoSize = true;
            this.liklblgg.BackColor = System.Drawing.Color.Transparent;
            this.liklblgg.DisabledLinkColor = System.Drawing.Color.Black;
            this.liklblgg.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.liklblgg.ForeColor = System.Drawing.Color.Black;
            this.liklblgg.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.liklblgg.LinkColor = System.Drawing.Color.Black;
            this.liklblgg.Location = new System.Drawing.Point(3, 10);
            this.liklblgg.Name = "liklblgg";
            this.liklblgg.Size = new System.Drawing.Size(74, 17);
            this.liklblgg.TabIndex = 130;
            this.liklblgg.TabStop = true;
            this.liklblgg.Text = "GG聊天程序";
            this.liklblgg.VisitedLinkColor = System.Drawing.SystemColors.HotTrack;
            // 
            // skinButtom3
            // 
            this.skinButtom3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.skinButtom3.BackColor = System.Drawing.Color.Transparent;
            this.skinButtom3.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.skinButtom3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButtom3.DownBack = ((System.Drawing.Image)(resources.GetObject("skinButtom3.DownBack")));
            this.skinButtom3.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.skinButtom3.Location = new System.Drawing.Point(396, 6);
            this.skinButtom3.Margin = new System.Windows.Forms.Padding(0);
            this.skinButtom3.MouseBack = ((System.Drawing.Image)(resources.GetObject("skinButtom3.MouseBack")));
            this.skinButtom3.Name = "skinButtom3";
            this.skinButtom3.NormlBack = ((System.Drawing.Image)(resources.GetObject("skinButtom3.NormlBack")));
            this.skinButtom3.Size = new System.Drawing.Size(20, 24);
            this.skinButtom3.TabIndex = 129;
            this.skinButtom3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.skinButtom3.UseVisualStyleBackColor = false;
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.BackColor = System.Drawing.Color.Transparent;
            this.btnSend.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.btnSend.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btnSend.DownBack = ((System.Drawing.Image)(resources.GetObject("btnSend.DownBack")));
            this.btnSend.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.btnSend.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSend.Location = new System.Drawing.Point(335, 6);
            this.btnSend.Margin = new System.Windows.Forms.Padding(0);
            this.btnSend.MouseBack = ((System.Drawing.Image)(resources.GetObject("btnSend.MouseBack")));
            this.btnSend.Name = "btnSend";
            this.btnSend.NormlBack = ((System.Drawing.Image)(resources.GetObject("btnSend.NormlBack")));
            this.btnSend.Size = new System.Drawing.Size(61, 24);
            this.btnSend.TabIndex = 128;
            this.btnSend.Text = "发送(&S)";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.btnClose.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.DownBack = ((System.Drawing.Image)(resources.GetObject("btnClose.DownBack")));
            this.btnClose.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.btnClose.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnClose.Location = new System.Drawing.Point(263, 6);
            this.btnClose.MouseBack = ((System.Drawing.Image)(resources.GetObject("btnClose.MouseBack")));
            this.btnClose.Name = "btnClose";
            this.btnClose.NormlBack = ((System.Drawing.Image)(resources.GetObject("btnClose.NormlBack")));
            this.btnClose.Size = new System.Drawing.Size(69, 24);
            this.btnClose.TabIndex = 127;
            this.btnClose.Text = "关闭(&C)";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel1.Controls.Add(this.liklblgg);
            this.skinPanel1.Controls.Add(this.btnSend);
            this.skinPanel1.Controls.Add(this.skinButtom3);
            this.skinPanel1.Controls.Add(this.btnClose);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Location = new System.Drawing.Point(4, 451);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(419, 35);
            this.skinPanel1.TabIndex = 127;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(4, 448);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(419, 3);
            this.splitter2.TabIndex = 128;
            this.splitter2.TabStop = false;
            this.splitter2.Visible = false;
            // 
            // skinPanel_left
            // 
            this.skinPanel_left.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel_left.Controls.Add(this.textBoxSend);
            this.skinPanel_left.Controls.Add(this.rtfRichTextBox_history);
            this.skinPanel_left.Controls.Add(this.skToolMenu);
            this.skinPanel_left.Controls.Add(this.skinToolStrip2);
            this.skinPanel_left.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel_left.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.skinPanel_left.DownBack = null;
            this.skinPanel_left.Location = new System.Drawing.Point(4, 58);
            this.skinPanel_left.MouseBack = null;
            this.skinPanel_left.Name = "skinPanel_left";
            this.skinPanel_left.NormlBack = null;
            this.skinPanel_left.Size = new System.Drawing.Size(419, 390);
            this.skinPanel_left.TabIndex = 129;
            // 
            // textBoxSend
            // 
            this.textBoxSend.BackColor = System.Drawing.Color.White;
            this.textBoxSend.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxSend.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxSend.EnableAutoDragDrop = true;
            this.textBoxSend.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textBoxSend.HiglightColor = CCWin.SkinControl.RtfRichTextBox.RtfColor.White;
            this.textBoxSend.Location = new System.Drawing.Point(0, 292);
            this.textBoxSend.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxSend.Name = "textBoxSend";
            this.textBoxSend.Size = new System.Drawing.Size(419, 98);
            this.textBoxSend.TabIndex = 123;
            this.textBoxSend.Text = "";
            this.textBoxSend.TextColor = CCWin.SkinControl.RtfRichTextBox.RtfColor.Black;
            // 
            // rtfRichTextBox_history
            // 
            this.rtfRichTextBox_history.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rtfRichTextBox_history.BackColor = System.Drawing.Color.White;
            this.rtfRichTextBox_history.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtfRichTextBox_history.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.rtfRichTextBox_history.HiglightColor = CCWin.SkinControl.RtfRichTextBox.RtfColor.White;
            this.rtfRichTextBox_history.Location = new System.Drawing.Point(0, 33);
            this.rtfRichTextBox_history.Margin = new System.Windows.Forms.Padding(0);
            this.rtfRichTextBox_history.Name = "rtfRichTextBox_history";
            this.rtfRichTextBox_history.ReadOnly = true;
            this.rtfRichTextBox_history.Size = new System.Drawing.Size(419, 232);
            this.rtfRichTextBox_history.TabIndex = 1;
            this.rtfRichTextBox_history.Text = "";
            this.rtfRichTextBox_history.TextColor = CCWin.SkinControl.RtfRichTextBox.RtfColor.Black;
            // 
            // skToolMenu
            // 
            this.skToolMenu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.skToolMenu.Arrow = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.skToolMenu.AutoSize = false;
            this.skToolMenu.Back = System.Drawing.Color.White;
            this.skToolMenu.BackColor = System.Drawing.Color.Transparent;
            this.skToolMenu.BackRadius = 4;
            this.skToolMenu.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.skToolMenu.Base = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.skToolMenu.BaseFore = System.Drawing.Color.Black;
            this.skToolMenu.BaseForeAnamorphosis = false;
            this.skToolMenu.BaseForeAnamorphosisBorder = 4;
            this.skToolMenu.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.skToolMenu.BaseHoverFore = System.Drawing.Color.Black;
            this.skToolMenu.BaseItemAnamorphosis = true;
            this.skToolMenu.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.skToolMenu.BaseItemBorderShow = true;
            this.skToolMenu.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("skToolMenu.BaseItemDown")));
            this.skToolMenu.BaseItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.skToolMenu.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("skToolMenu.BaseItemMouse")));
            this.skToolMenu.BaseItemPressed = System.Drawing.Color.Transparent;
            this.skToolMenu.BaseItemRadius = 2;
            this.skToolMenu.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skToolMenu.BaseItemSplitter = System.Drawing.Color.Transparent;
            this.skToolMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.skToolMenu.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skToolMenu.Fore = System.Drawing.Color.Black;
            this.skToolMenu.GripMargin = new System.Windows.Forms.Padding(2, 2, 4, 2);
            this.skToolMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.skToolMenu.HoverFore = System.Drawing.Color.White;
            this.skToolMenu.ItemAnamorphosis = false;
            this.skToolMenu.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skToolMenu.ItemBorderShow = false;
            this.skToolMenu.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skToolMenu.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skToolMenu.ItemRadius = 3;
            this.skToolMenu.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.None;
            this.skToolMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolFont,
            this.toolStripButtonEmotion,
            this.toolVibration,
            this.toolStripButton3,
            this.toolStripButton2,
            this.toolStripDropDownButton4,
            this.toolStripButton1});
            this.skToolMenu.Location = new System.Drawing.Point(0, 265);
            this.skToolMenu.Name = "skToolMenu";
            this.skToolMenu.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skToolMenu.Size = new System.Drawing.Size(417, 27);
            this.skToolMenu.TabIndex = 132;
            this.skToolMenu.Text = "skinToolStrip1";
            this.skToolMenu.TitleAnamorphosis = false;
            this.skToolMenu.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skToolMenu.TitleRadius = 4;
            this.skToolMenu.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolFont
            // 
            this.toolFont.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolFont.Image = global::GG.Properties.Resources.aio_quickbar_font;
            this.toolFont.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolFont.Margin = new System.Windows.Forms.Padding(4, 1, 0, 2);
            this.toolFont.Name = "toolFont";
            this.toolFont.Size = new System.Drawing.Size(23, 24);
            this.toolFont.Text = "toolStripButton1";
            this.toolFont.ToolTipText = "字体选择工具栏";
            this.toolFont.Click += new System.EventHandler(this.toolFont_Click);
            // 
            // toolStripButtonEmotion
            // 
            this.toolStripButtonEmotion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonEmotion.Image = global::GG.Properties.Resources.aio_quickbar_face;
            this.toolStripButtonEmotion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonEmotion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEmotion.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.toolStripButtonEmotion.Name = "toolStripButtonEmotion";
            this.toolStripButtonEmotion.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonEmotion.Text = "toolStripButton2";
            this.toolStripButtonEmotion.ToolTipText = "选择表情";
            this.toolStripButtonEmotion.Click += new System.EventHandler(this.toolStripButtonEmotion_Click);
            // 
            // toolVibration
            // 
            this.toolVibration.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolVibration.Image = global::GG.Properties.Resources.aio_quickbar_twitter;
            this.toolVibration.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolVibration.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolVibration.Margin = new System.Windows.Forms.Padding(3, 1, 0, 2);
            this.toolVibration.Name = "toolVibration";
            this.toolVibration.Size = new System.Drawing.Size(24, 24);
            this.toolVibration.Text = "toolStripButton4";
            this.toolVibration.ToolTipText = "向好友发送窗口抖动";
            this.toolVibration.Click += new System.EventHandler(this.toolVibration_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::GG.Properties.Resources.aio_quickbar_inputassist;
            this.toolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton3.Text = "手写板";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::GG.Properties.Resources.aio_quickbar_cut;
            this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton2.Text = "屏幕截图";
            this.toolStripButton2.Click += new System.EventHandler(this.buttonCapture_Click);
            // 
            // toolStripDropDownButton4
            // 
            this.toolStripDropDownButton4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripDropDownButton4.AutoSize = false;
            this.toolStripDropDownButton4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示消息记录ToolStripMenuItem,
            this.显示比例ToolStripMenuItem,
            this.toolStripMenuItem5,
            this.清屏ToolStripMenuItem,
            this.消息管理器ToolStripMenuItem});
            this.toolStripDropDownButton4.Image = global::GG.Properties.Resources.aio_quickbar_register;
            this.toolStripDropDownButton4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripDropDownButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripDropDownButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton4.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.toolStripDropDownButton4.Name = "toolStripDropDownButton4";
            this.toolStripDropDownButton4.Size = new System.Drawing.Size(90, 24);
            this.toolStripDropDownButton4.Text = "消息记录";
            this.toolStripDropDownButton4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripDropDownButton4.ToolTipText = "显示消息记录";
            // 
            // 显示消息记录ToolStripMenuItem
            // 
            this.显示消息记录ToolStripMenuItem.Name = "显示消息记录ToolStripMenuItem";
            this.显示消息记录ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.显示消息记录ToolStripMenuItem.Text = "显示消息记录";
            // 
            // 显示比例ToolStripMenuItem
            // 
            this.显示比例ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.放大Ctrl滚轮ToolStripMenuItem,
            this.缩小ToolStripMenuItem,
            this.toolStripMenuItem6,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.toolStripMenuItem10,
            this.toolStripMenuItem11,
            this.toolStripMenuItem12,
            this.toolStripMenuItem13,
            this.toolStripMenuItem14});
            this.显示比例ToolStripMenuItem.Name = "显示比例ToolStripMenuItem";
            this.显示比例ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.显示比例ToolStripMenuItem.Text = "显示比例";
            // 
            // 放大Ctrl滚轮ToolStripMenuItem
            // 
            this.放大Ctrl滚轮ToolStripMenuItem.Name = "放大Ctrl滚轮ToolStripMenuItem";
            this.放大Ctrl滚轮ToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.放大Ctrl滚轮ToolStripMenuItem.Text = "放大 Ctrl+滚轮";
            // 
            // 缩小ToolStripMenuItem
            // 
            this.缩小ToolStripMenuItem.Name = "缩小ToolStripMenuItem";
            this.缩小ToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.缩小ToolStripMenuItem.Text = "缩小";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(154, 6);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem8.Text = "400%";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem9.Text = "200%";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem10.Text = "150%";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem11.Text = "125%";
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Image = global::GG.Properties.Resources.menu_check;
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem12.Text = "100%";
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem13.Text = "75%";
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem14.Text = "50%";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(145, 6);
            // 
            // 清屏ToolStripMenuItem
            // 
            this.清屏ToolStripMenuItem.Name = "清屏ToolStripMenuItem";
            this.清屏ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.清屏ToolStripMenuItem.Text = "清屏";
            // 
            // 消息管理器ToolStripMenuItem
            // 
            this.消息管理器ToolStripMenuItem.Name = "消息管理器ToolStripMenuItem";
            this.消息管理器ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.消息管理器ToolStripMenuItem.Text = "消息管理器";
            // 
            // skinToolStrip2
            // 
            this.skinToolStrip2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.skinToolStrip2.Arrow = System.Drawing.Color.White;
            this.skinToolStrip2.AutoSize = false;
            this.skinToolStrip2.Back = System.Drawing.Color.White;
            this.skinToolStrip2.BackColor = System.Drawing.Color.Transparent;
            this.skinToolStrip2.BackRadius = 4;
            this.skinToolStrip2.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.skinToolStrip2.Base = System.Drawing.Color.Transparent;
            this.skinToolStrip2.BaseFore = System.Drawing.Color.Black;
            this.skinToolStrip2.BaseForeAnamorphosis = false;
            this.skinToolStrip2.BaseForeAnamorphosisBorder = 4;
            this.skinToolStrip2.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.skinToolStrip2.BaseHoverFore = System.Drawing.Color.White;
            this.skinToolStrip2.BaseItemAnamorphosis = true;
            this.skinToolStrip2.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(93)))), ((int)(((byte)(93)))));
            this.skinToolStrip2.BaseItemBorderShow = true;
            this.skinToolStrip2.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("skinToolStrip2.BaseItemDown")));
            this.skinToolStrip2.BaseItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.skinToolStrip2.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("skinToolStrip2.BaseItemMouse")));
            this.skinToolStrip2.BaseItemPressed = System.Drawing.Color.Transparent;
            this.skinToolStrip2.BaseItemRadius = 2;
            this.skinToolStrip2.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip2.BaseItemSplitter = System.Drawing.Color.Transparent;
            this.skinToolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.skinToolStrip2.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinToolStrip2.Fore = System.Drawing.Color.Black;
            this.skinToolStrip2.GripMargin = new System.Windows.Forms.Padding(2, 2, 4, 2);
            this.skinToolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.skinToolStrip2.HoverFore = System.Drawing.Color.White;
            this.skinToolStrip2.ItemAnamorphosis = false;
            this.skinToolStrip2.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip2.ItemBorderShow = false;
            this.skinToolStrip2.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip2.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip2.ItemRadius = 1;
            this.skinToolStrip2.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.None;
            this.skinToolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton5,
            this.ToolFile,
            this.toolStripButton4,
            this.toolStripSplitButton2,
            this.toolStripSplitButton1});
            this.skinToolStrip2.Location = new System.Drawing.Point(4, 0);
            this.skinToolStrip2.Name = "skinToolStrip2";
            this.skinToolStrip2.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip2.Size = new System.Drawing.Size(412, 33);
            this.skinToolStrip2.TabIndex = 133;
            this.skinToolStrip2.Text = "skinToolStrip2";
            this.skinToolStrip2.TitleAnamorphosis = false;
            this.skinToolStrip2.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinToolStrip2.TitleRadius = 4;
            this.skinToolStrip2.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(24, 30);
            this.toolStripButton5.Text = "toolStripButton5";
            this.toolStripButton5.ToolTipText = "视频会话";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripDropDownButton1_ButtonClick);
            // 
            // ToolFile
            // 
            this.ToolFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem32,
            this.toolStripMenuItem33,
            this.toolStripMenuItem34});
            this.ToolFile.Image = ((System.Drawing.Image)(resources.GetObject("ToolFile.Image")));
            this.ToolFile.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolFile.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.ToolFile.Name = "ToolFile";
            this.ToolFile.Size = new System.Drawing.Size(36, 30);
            this.ToolFile.Text = "toolStripButton8";
            this.ToolFile.ToolTipText = "发送文件";
            // 
            // toolStripMenuItem32
            // 
            this.toolStripMenuItem32.Name = "toolStripMenuItem32";
            this.toolStripMenuItem32.Size = new System.Drawing.Size(148, 22);
            this.toolStripMenuItem32.Text = "发送文件";
            this.toolStripMenuItem32.Click += new System.EventHandler(this.toolStripButton_fileTransfer_Click);
            // 
            // toolStripMenuItem33
            // 
            this.toolStripMenuItem33.Name = "toolStripMenuItem33";
            this.toolStripMenuItem33.Size = new System.Drawing.Size(148, 22);
            this.toolStripMenuItem33.Text = "发送文件夹";
            this.toolStripMenuItem33.Click += new System.EventHandler(this.toolStripMenuItem33_Click);
            // 
            // toolStripMenuItem34
            // 
            this.toolStripMenuItem34.Name = "toolStripMenuItem34";
            this.toolStripMenuItem34.Size = new System.Drawing.Size(148, 22);
            this.toolStripMenuItem34.Text = "发送离线文件";
            this.toolStripMenuItem34.Click += new System.EventHandler(this.toolStripMenuItem34_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = global::GG.Properties.Resources.Boot1;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 31);
            this.toolStripButton4.Text = "远程磁盘";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripSplitButton2
            // 
            this.toolStripSplitButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.请求远程协助ToolStripMenuItem,
            this.桌面共享指定区域ToolStripMenuItem});
            this.toolStripSplitButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton2.Image")));
            this.toolStripSplitButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripSplitButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton2.Name = "toolStripSplitButton2";
            this.toolStripSplitButton2.Size = new System.Drawing.Size(36, 30);
            this.toolStripSplitButton2.Text = "远程协助、桌面共享";
            // 
            // 请求远程协助ToolStripMenuItem
            // 
            this.请求远程协助ToolStripMenuItem.Name = "请求远程协助ToolStripMenuItem";
            this.请求远程协助ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.请求远程协助ToolStripMenuItem.Text = "请求远程协助";
            this.请求远程协助ToolStripMenuItem.Click += new System.EventHandler(this.请求远程协助ToolStripMenuItem_Click);
            // 
            // 桌面共享指定区域ToolStripMenuItem
            // 
            this.桌面共享指定区域ToolStripMenuItem.Name = "桌面共享指定区域ToolStripMenuItem";
            this.桌面共享指定区域ToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.桌面共享指定区域ToolStripMenuItem.Text = "桌面共享（指定区域）";
            this.桌面共享指定区域ToolStripMenuItem.Click += new System.EventHandler(this.桌面共享指定区域ToolStripMenuItem_Click);
            // 
            // skinLabel_p2PState
            // 
            this.skinLabel_p2PState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.skinLabel_p2PState.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.Anamorphosis;
            this.skinLabel_p2PState.AutoSize = true;
            this.skinLabel_p2PState.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel_p2PState.BorderColor = System.Drawing.Color.White;
            this.skinLabel_p2PState.BorderSize = 4;
            this.skinLabel_p2PState.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.skinLabel_p2PState.ForeColor = System.Drawing.Color.IndianRed;
            this.skinLabel_p2PState.Location = new System.Drawing.Point(326, 32);
            this.skinLabel_p2PState.Name = "skinLabel_p2PState";
            this.skinLabel_p2PState.Size = new System.Drawing.Size(53, 17);
            this.skinLabel_p2PState.TabIndex = 100;
            this.skinLabel_p2PState.Text = "P2P直连";
            // 
            // pictureBox_state
            // 
            this.pictureBox_state.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_state.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_state.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox_state.Image = global::GG.Properties.Resources._4;
            this.pictureBox_state.Location = new System.Drawing.Point(307, 32);
            this.pictureBox_state.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox_state.Name = "pictureBox_state";
            this.pictureBox_state.Size = new System.Drawing.Size(16, 16);
            this.pictureBox_state.TabIndex = 123;
            this.pictureBox_state.TabStop = false;
            this.pictureBox_state.Visible = false;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 24);
            this.toolStripButton1.Text = "语音消息";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(32, 30);
            this.toolStripSplitButton1.Text = "来自对方的语音消息";
            // 
            // ChatForm
            // 
            this.AcceptButton = this.btnSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Back = ((System.Drawing.Image)(resources.GetObject("$this.Back")));
            this.BackLayout = false;
            this.BorderPalace = global::GG.Properties.Resources.BackPalace;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(630, 490);
            this.CloseBoxSize = new System.Drawing.Size(39, 20);
            this.CloseDownBack = global::GG.Properties.Resources.btn_close_down;
            this.CloseMouseBack = global::GG.Properties.Resources.btn_close_highlight;
            this.CloseNormlBack = global::GG.Properties.Resources.btn_close_disable;
            this.ControlBoxOffset = new System.Drawing.Point(0, -1);
            this.Controls.Add(this.skinPanel_left);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.skinPanel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.skinPanel_right);
            this.Controls.Add(this.pictureBox_state);
            this.Controls.Add(this.imgyy2);
            this.Controls.Add(this.pnlTx);
            this.Controls.Add(this.skinLabel_p2PState);
            this.Controls.Add(this.labelFriendName);
            this.Controls.Add(this.labelFriendSignature);
            this.EffectCaption = false;
            this.EffectWidth = 4;
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaxDownBack = global::GG.Properties.Resources.btn_max_down;
            this.MaxMouseBack = global::GG.Properties.Resources.btn_max_highlight;
            this.MaxNormlBack = global::GG.Properties.Resources.btn_max_normal;
            this.MaxSize = new System.Drawing.Size(28, 20);
            this.MiniDownBack = global::GG.Properties.Resources.btn_mini_down;
            this.MiniMouseBack = global::GG.Properties.Resources.btn_mini_highlight;
            this.MinimumSize = new System.Drawing.Size(630, 490);
            this.MiniNormlBack = global::GG.Properties.Resources.btn_mini_normal;
            this.MiniSize = new System.Drawing.Size(28, 20);
            this.Name = "ChatForm";
            this.RestoreDownBack = global::GG.Properties.Resources.btn_restore_down;
            this.RestoreMouseBack = global::GG.Properties.Resources.btn_restore_highlight;
            this.RestoreNormlBack = global::GG.Properties.Resources.btn_restore_normal;
            this.Shadow = false;
            this.ShowBorder = false;
            this.ShowDrawIcon = false;
            this.Special = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.SysBottomDown = global::GG.Properties.Resources.AIO_SetBtn_down;
            this.SysBottomMouse = global::GG.Properties.Resources.AIO_SetBtn_highlight;
            this.SysBottomNorml = global::GG.Properties.Resources.AIO_SetBtn_normal;
            this.SysBottomVisibale = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChatForm_FormClosing);
            this.SizeChanged += new System.EventHandler(this.ChatForm_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FrmChat_Paint);
            this.pnlTx.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgyy2)).EndInit();
            this.skinPanel_right.ResumeLayout(false);
            this.skinTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgMeShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgChatShow)).EndInit();
            this.skinPanel1.ResumeLayout(false);
            this.skinPanel1.PerformLayout();
            this.skinPanel_left.ResumeLayout(false);
            this.skToolMenu.ResumeLayout(false);
            this.skToolMenu.PerformLayout();
            this.skinToolStrip2.ResumeLayout(false);
            this.skinToolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_state)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private CCWin.SkinControl.SkinLabel labelFriendName;
        private CCWin.SkinControl.SkinLabel labelFriendSignature;
        private CCWin.SkinControl.SkinPanel pnlTx;
        private CCWin.SkinControl.SkinPanel panelFriendHeadImage;
        private System.Windows.Forms.ToolTip toolShow;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.PictureBox imgyy2;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.ImageList imageList1;
        private CCWin.SkinControl.SkinPanel skinPanel_right;
        private CCWin.SkinControl.SkinTabControl skinTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox imgMeShow;
        private System.Windows.Forms.PictureBox imgChatShow;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.LinkLabel liklblgg;
        private CCWin.SkinControl.SkinButton skinButtom3;
        private CCWin.SkinControl.SkinButton btnSend;
        private CCWin.SkinControl.SkinButton btnClose;
        private CCWin.SkinControl.SkinPanel skinPanel1;
        private System.Windows.Forms.Splitter splitter2;
        private CCWin.SkinControl.SkinPanel skinPanel_left;
        private CCWin.SkinControl.RtfRichTextBox textBoxSend;
        private CCWin.SkinControl.RtfRichTextBox rtfRichTextBox_history;
        private CCWin.SkinControl.SkinToolStrip skToolMenu;
        private System.Windows.Forms.ToolStripButton toolFont;
        private System.Windows.Forms.ToolStripButton toolStripButtonEmotion;
        private System.Windows.Forms.ToolStripButton toolVibration;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSplitButton toolStripDropDownButton4;
        private System.Windows.Forms.ToolStripMenuItem 显示消息记录ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 显示比例ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 放大Ctrl滚轮ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 缩小ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem 清屏ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 消息管理器ToolStripMenuItem;
        private CCWin.SkinControl.SkinToolStrip skinToolStrip2;
        private System.Windows.Forms.ToolStripSplitButton ToolFile;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem32;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem33;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem34;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private CCWin.SkinControl.SkinLabel skinLabel_p2PState;
        private System.Windows.Forms.PictureBox pictureBox_state;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton2;
        private System.Windows.Forms.ToolStripMenuItem 请求远程协助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 桌面共享指定区域ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
    }
}