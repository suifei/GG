﻿namespace GG
{
    partial class FileTransferItem
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileTransferItem));
            this.linkLabel_cancel = new System.Windows.Forms.LinkLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.linkLabel_receive = new System.Windows.Forms.LinkLabel();
            this.skinLabel_FileName = new CCWin.SkinControl.SkinLabel();
            this.label_info = new CCWin.SkinControl.SkinLabel();
            this.skinProgressBar1 = new CCWin.SkinControl.SkinProgressBar();
            this.skinLabel_speedTitle = new CCWin.SkinControl.SkinLabel();
            this.label_speed = new CCWin.SkinControl.SkinLabel();
            this.label_fileSize = new CCWin.SkinControl.SkinLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // linkLabel_cancel
            // 
            resources.ApplyResources(this.linkLabel_cancel, "linkLabel_cancel");
            this.linkLabel_cancel.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel_cancel.LinkColor = System.Drawing.Color.CornflowerBlue;
            this.linkLabel_cancel.Name = "linkLabel_cancel";
            this.linkLabel_cancel.TabStop = true;
            this.linkLabel_cancel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // linkLabel_receive
            // 
            resources.ApplyResources(this.linkLabel_receive, "linkLabel_receive");
            this.linkLabel_receive.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel_receive.LinkColor = System.Drawing.Color.CornflowerBlue;
            this.linkLabel_receive.Name = "linkLabel_receive";
            this.linkLabel_receive.TabStop = true;
            this.linkLabel_receive.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_receive_LinkClicked);
            // 
            // skinLabel_FileName
            // 
            resources.ApplyResources(this.skinLabel_FileName, "skinLabel_FileName");
            this.skinLabel_FileName.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel_FileName.BorderColor = System.Drawing.Color.White;
            this.skinLabel_FileName.Name = "skinLabel_FileName";
            // 
            // label_info
            // 
            resources.ApplyResources(this.label_info, "label_info");
            this.label_info.BackColor = System.Drawing.Color.Transparent;
            this.label_info.BorderColor = System.Drawing.Color.White;
            this.label_info.ForeColor = System.Drawing.Color.Gray;
            this.label_info.Name = "label_info";
            // 
            // skinProgressBar1
            // 
            resources.ApplyResources(this.skinProgressBar1, "skinProgressBar1");
            this.skinProgressBar1.Back = null;
            this.skinProgressBar1.BackColor = System.Drawing.Color.White;
            this.skinProgressBar1.BarBack = null;
            this.skinProgressBar1.Border = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.skinProgressBar1.ForeColor = System.Drawing.Color.Peru;
            this.skinProgressBar1.Name = "skinProgressBar1";
            this.skinProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.skinProgressBar1.TrackBack = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            // 
            // skinLabel_speedTitle
            // 
            resources.ApplyResources(this.skinLabel_speedTitle, "skinLabel_speedTitle");
            this.skinLabel_speedTitle.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel_speedTitle.BorderColor = System.Drawing.Color.White;
            this.skinLabel_speedTitle.Name = "skinLabel_speedTitle";
            // 
            // label_speed
            // 
            resources.ApplyResources(this.label_speed, "label_speed");
            this.label_speed.BackColor = System.Drawing.Color.Transparent;
            this.label_speed.BorderColor = System.Drawing.Color.White;
            this.label_speed.Name = "label_speed";
            // 
            // label_fileSize
            // 
            resources.ApplyResources(this.label_fileSize, "label_fileSize");
            this.label_fileSize.BackColor = System.Drawing.Color.Transparent;
            this.label_fileSize.BorderColor = System.Drawing.Color.White;
            this.label_fileSize.Name = "label_fileSize";
            // 
            // FileTransferItem
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label_fileSize);
            this.Controls.Add(this.skinProgressBar1);
            this.Controls.Add(this.label_speed);
            this.Controls.Add(this.skinLabel_speedTitle);
            this.Controls.Add(this.label_info);
            this.Controls.Add(this.skinLabel_FileName);
            this.Controls.Add(this.linkLabel_receive);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.linkLabel_cancel);
            this.Name = "FileTransferItem";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkLabel_cancel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkLabel_receive;
        private CCWin.SkinControl.SkinLabel skinLabel_FileName;
        private CCWin.SkinControl.SkinLabel label_info;
        private CCWin.SkinControl.SkinProgressBar skinProgressBar1;
        private CCWin.SkinControl.SkinLabel skinLabel_speedTitle;
        private CCWin.SkinControl.SkinLabel label_speed;
        private CCWin.SkinControl.SkinLabel label_fileSize;
    }
}
