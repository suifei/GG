﻿using System;
using System.Collections.Generic;
using System.Text;
using ESBasic.ObjectManagement.Managers;
using GG.Core;
using ESPlus.Rapid;

namespace GG
{
    /// <summary>
    /// 缓存自己的所有好友、组友的资料。
    /// 建议：将所有的好友资料缓存在本地的持久化文件中，如此可避免每次启动时都从服务器加载所有好友信息，以减低服务器的压力和加快启动速度。
    /// </summary>
    public class GlobalUserCache
    {
        private ObjectManager<string, User> userCache = new ObjectManager<string, User>(); //缓存用户资料
        private IRapidPassiveEngine rapidPassiveEngine;
        public GlobalUserCache( IRapidPassiveEngine engine)
        {
            this.rapidPassiveEngine = engine;          
        }

        public User GetUser(string userID)
        {
            User user = this.userCache.Get(userID);
            if (user == null)
            {
                byte[] bUser = this.rapidPassiveEngine.CustomizeOutter.Query(InformationTypes.GetUserInfo, System.Text.Encoding.UTF8.GetBytes(userID));
                user = (User)GGHelper.DeserializeBytes(bUser, 0, bUser.Length);
                this.userCache.Add(userID, user);
            }
            return user;
        }

        public void Add(User user)
        {
            this.userCache.Add(user.ID, user);
        }
    }
}
