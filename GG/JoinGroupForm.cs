﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CCWin;
using CCWin.Win32;
using CCWin.Win32.Const;
using System.Diagnostics;
using GG.Core;
using System.Configuration;
using ESPlus.Rapid;

namespace GG
{
    public partial class JoinGroupForm : CCSkinMain
    {       
        private IRapidPassiveEngine rapidPassiveEngine;

        public JoinGroupForm(IRapidPassiveEngine engine)
        {
            InitializeComponent();
            this.rapidPassiveEngine = engine;
        }

        #region GroupID
        private string groupID = null;
        public string GroupID
        {
            get
            {
                return this.groupID;
            }
        } 
        #endregion        

        private void skinButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.groupID = this.skinTextBox_id.SkinTxt.Text.Trim();
            if (groupID.Length == 0)
            {
                MessageBox.Show("群帐号不能为空！");
                return;
            }

            try
            {
                byte[] bRes = this.rapidPassiveEngine.CustomizeOutter.Query(InformationTypes.JoinGroup, System.Text.Encoding.UTF8.GetBytes(groupID));
                JoinGroupResult res = (JoinGroupResult)BitConverter.ToInt32(bRes, 0);
                if (res == JoinGroupResult.GrpupNotExist)
                {
                    MessageBox.Show("群不存在！");
                    return;
                }

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception ee)
            {
                MessageBox.Show("加入群失败！" + ee.Message);
            }
        }

        private void JoinGroupForm_Load(object sender, EventArgs e)
        {

        }      
         
    }
}
