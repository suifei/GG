﻿namespace GG
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            CCWin.SkinControl.ChatListItem chatListItem1 = new CCWin.SkinControl.ChatListItem();
            CCWin.SkinControl.ChatListItem chatListItem2 = new CCWin.SkinControl.ChatListItem();
            CCWin.SkinControl.ChatListItem chatListItem3 = new CCWin.SkinControl.ChatListItem();
            CCWin.SkinControl.ChatListItem chatListItem4 = new CCWin.SkinControl.ChatListItem();
            CCWin.SkinControl.ChatListItem chatListItem5 = new CCWin.SkinControl.ChatListItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.chatListBox = new CCWin.SkinControl.ChatListBox();
            this.skinContextMenuStrip_user = new CCWin.SkinControl.SkinContextMenuStrip();
            this.toolStripMenuItem51 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem60 = new System.Windows.Forms.ToolStripSeparator();
            this.移动联系人至ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.我的好友ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.自己ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.黑名单ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除好友ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.修改备注名ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem53 = new System.Windows.Forms.ToolStripSeparator();
            this.消息记录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.查看本地消息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.查看漫游消息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.查看上传消息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.查看资料ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chatListBox_group = new CCWin.SkinControl.ChatListBox();
            this.skinContextMenuStrip_Group = new CCWin.SkinControl.SkinContextMenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.chatListBox_recent = new CCWin.SkinControl.ChatListBox();
            this.labelSignature = new CCWin.SkinControl.SkinLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.skinToolStrip1 = new CCWin.SkinControl.SkinToolStrip();
            this.toolstripButton_mainMenu = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton19 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.labelName = new CCWin.SkinControl.SkinLabel();
            this.skinButton_State = new CCWin.SkinControl.SkinButton();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.skinContextMenuStrip_State = new CCWin.SkinControl.SkinContextMenuStrip();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem33 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem39 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem40 = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolStripSplitButton4 = new System.Windows.Forms.ToolStripSplitButton();
            this.添加好友ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSplitButton3 = new System.Windows.Forms.ToolStripSplitButton();
            this.加入群ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.创建群ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSplitButton2 = new System.Windows.Forms.ToolStripSplitButton();
            this.清空会话列表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.skinToolStrip3 = new CCWin.SkinControl.SkinToolStrip();
            this.skinPanel_HeadImage = new CCWin.SkinControl.SkinPanel();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.skinContextMenuStrip_mainMenu = new CCWin.SkinControl.SkinContextMenuStrip();
            this.修改密码ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolQQShow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolExit = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlTx = new CCWin.SkinControl.SkinPanel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.skinContextMenuStrip_user.SuspendLayout();
            this.skinContextMenuStrip_Group.SuspendLayout();
            this.skinToolStrip1.SuspendLayout();
            this.skinContextMenuStrip_State.SuspendLayout();
            this.skinToolStrip3.SuspendLayout();
            this.skinContextMenuStrip_mainMenu.SuspendLayout();
            this.pnlTx.SuspendLayout();
            this.SuspendLayout();
            // 
            // chatListBox
            // 
            this.chatListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.chatListBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(247)))), ((int)(((byte)(250)))));
            this.chatListBox.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chatListBox.ForeColor = System.Drawing.Color.Black;
            chatListItem1.Bounds = new System.Drawing.Rectangle(0, 1, 278, 25);
            chatListItem1.IsTwinkleHide = false;
            chatListItem1.OwnerChatListBox = this.chatListBox;
            chatListItem1.Text = "自己";
            chatListItem1.TwinkleSubItemNumber = 0;
            chatListItem2.Bounds = new System.Drawing.Rectangle(0, 27, 278, 25);
            chatListItem2.IsTwinkleHide = false;
            chatListItem2.OwnerChatListBox = this.chatListBox;
            chatListItem2.Text = "我的好友";
            chatListItem2.TwinkleSubItemNumber = 0;
            chatListItem3.Bounds = new System.Drawing.Rectangle(0, 53, 278, 25);
            chatListItem3.IsTwinkleHide = false;
            chatListItem3.OwnerChatListBox = this.chatListBox;
            chatListItem3.Text = "黑名单";
            chatListItem3.TwinkleSubItemNumber = 0;
            this.chatListBox.Items.AddRange(new CCWin.SkinControl.ChatListItem[] {
            chatListItem1,
            chatListItem2,
            chatListItem3});
            this.chatListBox.ListHadOpenGroup = null;
            this.chatListBox.ListSubItemMenu = this.skinContextMenuStrip_user;
            this.chatListBox.Location = new System.Drawing.Point(1, 136);
            this.chatListBox.Margin = new System.Windows.Forms.Padding(0);
            this.chatListBox.Name = "chatListBox";
            this.chatListBox.SelectSubItem = null;
            this.chatListBox.Size = new System.Drawing.Size(278, 477);
            this.chatListBox.SubItemMenu = null;
            this.chatListBox.TabIndex = 2;
            this.chatListBox.DoubleClickSubItem += new CCWin.SkinControl.ChatListBox.ChatListEventHandler(this.chatShow_DoubleClickSubItem);
            this.chatListBox.MouseEnterHead += new CCWin.SkinControl.ChatListBox.ChatListEventHandler(this.chatShow_MouseEnterHead);
            this.chatListBox.MouseLeaveHead += new CCWin.SkinControl.ChatListBox.ChatListEventHandler(this.chatShow_MouseLeaveHead);
            // 
            // skinContextMenuStrip_user
            // 
            this.skinContextMenuStrip_user.Arrow = System.Drawing.Color.Gray;
            this.skinContextMenuStrip_user.Back = System.Drawing.Color.White;
            this.skinContextMenuStrip_user.BackRadius = 4;
            this.skinContextMenuStrip_user.Base = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(200)))), ((int)(((byte)(254)))));
            this.skinContextMenuStrip_user.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinContextMenuStrip_user.Fore = System.Drawing.Color.Black;
            this.skinContextMenuStrip_user.HoverFore = System.Drawing.Color.White;
            this.skinContextMenuStrip_user.ImageScalingSize = new System.Drawing.Size(11, 11);
            this.skinContextMenuStrip_user.ItemAnamorphosis = false;
            this.skinContextMenuStrip_user.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinContextMenuStrip_user.ItemBorderShow = false;
            this.skinContextMenuStrip_user.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinContextMenuStrip_user.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinContextMenuStrip_user.ItemRadius = 4;
            this.skinContextMenuStrip_user.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.None;
            this.skinContextMenuStrip_user.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem51,
            this.toolStripMenuItem60,
            this.移动联系人至ToolStripMenuItem,
            this.删除好友ToolStripMenuItem,
            this.修改备注名ToolStripMenuItem,
            this.toolStripMenuItem53,
            this.消息记录ToolStripMenuItem,
            this.查看资料ToolStripMenuItem});
            this.skinContextMenuStrip_user.ItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinContextMenuStrip_user.Name = "MenuState";
            this.skinContextMenuStrip_user.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinContextMenuStrip_user.Size = new System.Drawing.Size(149, 148);
            this.skinContextMenuStrip_user.TitleAnamorphosis = false;
            this.skinContextMenuStrip_user.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinContextMenuStrip_user.TitleRadius = 4;
            this.skinContextMenuStrip_user.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolStripMenuItem51
            // 
            this.toolStripMenuItem51.Name = "toolStripMenuItem51";
            this.toolStripMenuItem51.Size = new System.Drawing.Size(148, 22);
            this.toolStripMenuItem51.Tag = "1";
            this.toolStripMenuItem51.Text = "发送即时消息";
            this.toolStripMenuItem51.ToolTipText = "表示希望好友看到您在线。\r\n声音：开启\r\n消息提醒框：开启\r\n会话消息：任务栏头像闪动\r\n";
            this.toolStripMenuItem51.Click += new System.EventHandler(this.toolStripMenuItem51_Click);
            // 
            // toolStripMenuItem60
            // 
            this.toolStripMenuItem60.Name = "toolStripMenuItem60";
            this.toolStripMenuItem60.Size = new System.Drawing.Size(145, 6);
            // 
            // 移动联系人至ToolStripMenuItem
            // 
            this.移动联系人至ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.我的好友ToolStripMenuItem,
            this.自己ToolStripMenuItem,
            this.黑名单ToolStripMenuItem});
            this.移动联系人至ToolStripMenuItem.Name = "移动联系人至ToolStripMenuItem";
            this.移动联系人至ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.移动联系人至ToolStripMenuItem.Text = "移动联系人至";
            // 
            // 我的好友ToolStripMenuItem
            // 
            this.我的好友ToolStripMenuItem.Name = "我的好友ToolStripMenuItem";
            this.我的好友ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.我的好友ToolStripMenuItem.Text = "我的好友";
            // 
            // 自己ToolStripMenuItem
            // 
            this.自己ToolStripMenuItem.Name = "自己ToolStripMenuItem";
            this.自己ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.自己ToolStripMenuItem.Text = "自己";
            // 
            // 黑名单ToolStripMenuItem
            // 
            this.黑名单ToolStripMenuItem.Name = "黑名单ToolStripMenuItem";
            this.黑名单ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.黑名单ToolStripMenuItem.Text = "黑名单";
            // 
            // 删除好友ToolStripMenuItem
            // 
            this.删除好友ToolStripMenuItem.Name = "删除好友ToolStripMenuItem";
            this.删除好友ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.删除好友ToolStripMenuItem.Text = "删除好友";
            this.删除好友ToolStripMenuItem.Click += new System.EventHandler(this.删除好友ToolStripMenuItem_Click);
            // 
            // 修改备注名ToolStripMenuItem
            // 
            this.修改备注名ToolStripMenuItem.Name = "修改备注名ToolStripMenuItem";
            this.修改备注名ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.修改备注名ToolStripMenuItem.Text = "修改备注名";
            // 
            // toolStripMenuItem53
            // 
            this.toolStripMenuItem53.Name = "toolStripMenuItem53";
            this.toolStripMenuItem53.Size = new System.Drawing.Size(145, 6);
            // 
            // 消息记录ToolStripMenuItem
            // 
            this.消息记录ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查看本地消息ToolStripMenuItem,
            this.查看漫游消息ToolStripMenuItem,
            this.查看上传消息ToolStripMenuItem});
            this.消息记录ToolStripMenuItem.Name = "消息记录ToolStripMenuItem";
            this.消息记录ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.消息记录ToolStripMenuItem.Text = "消息记录";
            // 
            // 查看本地消息ToolStripMenuItem
            // 
            this.查看本地消息ToolStripMenuItem.Name = "查看本地消息ToolStripMenuItem";
            this.查看本地消息ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.查看本地消息ToolStripMenuItem.Text = "查看本地消息";
            // 
            // 查看漫游消息ToolStripMenuItem
            // 
            this.查看漫游消息ToolStripMenuItem.Name = "查看漫游消息ToolStripMenuItem";
            this.查看漫游消息ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.查看漫游消息ToolStripMenuItem.Text = "查看漫游消息";
            // 
            // 查看上传消息ToolStripMenuItem
            // 
            this.查看上传消息ToolStripMenuItem.Name = "查看上传消息ToolStripMenuItem";
            this.查看上传消息ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.查看上传消息ToolStripMenuItem.Text = "查看上传消息";
            // 
            // 查看资料ToolStripMenuItem
            // 
            this.查看资料ToolStripMenuItem.Name = "查看资料ToolStripMenuItem";
            this.查看资料ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.查看资料ToolStripMenuItem.Text = "查看资料";
            // 
            // chatListBox_group
            // 
            this.chatListBox_group.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.chatListBox_group.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(247)))), ((int)(((byte)(250)))));
            this.chatListBox_group.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chatListBox_group.ForeColor = System.Drawing.Color.Black;
            chatListItem4.Bounds = new System.Drawing.Rectangle(0, 1, 278, 25);
            chatListItem4.IsTwinkleHide = false;
            chatListItem4.OwnerChatListBox = this.chatListBox_group;
            chatListItem4.Text = "我的GG群";
            chatListItem4.TwinkleSubItemNumber = 0;
            this.chatListBox_group.Items.AddRange(new CCWin.SkinControl.ChatListItem[] {
            chatListItem4});
            this.chatListBox_group.ListHadOpenGroup = null;
            this.chatListBox_group.ListSubItemMenu = this.skinContextMenuStrip_Group;
            this.chatListBox_group.Location = new System.Drawing.Point(1, 136);
            this.chatListBox_group.Margin = new System.Windows.Forms.Padding(0);
            this.chatListBox_group.Name = "chatListBox_group";
            this.chatListBox_group.SelectSubItem = null;
            this.chatListBox_group.Size = new System.Drawing.Size(278, 477);
            this.chatListBox_group.SubItemMenu = null;
            this.chatListBox_group.TabIndex = 132;
            this.chatListBox_group.Visible = false;
            this.chatListBox_group.DoubleClickSubItem += new CCWin.SkinControl.ChatListBox.ChatListEventHandler(this.chatListBox_group_DoubleClickSubItem);
            // 
            // skinContextMenuStrip_Group
            // 
            this.skinContextMenuStrip_Group.Arrow = System.Drawing.Color.Black;
            this.skinContextMenuStrip_Group.Back = System.Drawing.Color.White;
            this.skinContextMenuStrip_Group.BackRadius = 4;
            this.skinContextMenuStrip_Group.Base = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(200)))), ((int)(((byte)(254)))));
            this.skinContextMenuStrip_Group.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinContextMenuStrip_Group.Fore = System.Drawing.Color.Black;
            this.skinContextMenuStrip_Group.HoverFore = System.Drawing.Color.White;
            this.skinContextMenuStrip_Group.ItemAnamorphosis = true;
            this.skinContextMenuStrip_Group.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinContextMenuStrip_Group.ItemBorderShow = true;
            this.skinContextMenuStrip_Group.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinContextMenuStrip_Group.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinContextMenuStrip_Group.ItemRadius = 4;
            this.skinContextMenuStrip_Group.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinContextMenuStrip_Group.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.skinContextMenuStrip_Group.ItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinContextMenuStrip_Group.Name = "skinContextMenuStrip1";
            this.skinContextMenuStrip_Group.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinContextMenuStrip_Group.Size = new System.Drawing.Size(125, 26);
            this.skinContextMenuStrip_Group.TitleAnamorphosis = true;
            this.skinContextMenuStrip_Group.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinContextMenuStrip_Group.TitleRadius = 4;
            this.skinContextMenuStrip_Group.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem1.Text = "退出该群";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // chatListBox_recent
            // 
            this.chatListBox_recent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.chatListBox_recent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(247)))), ((int)(((byte)(250)))));
            this.chatListBox_recent.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chatListBox_recent.ForeColor = System.Drawing.Color.Black;
            chatListItem5.Bounds = new System.Drawing.Rectangle(0, 1, 278, 25);
            chatListItem5.IsOpen = true;
            chatListItem5.IsTwinkleHide = false;
            chatListItem5.OwnerChatListBox = this.chatListBox_recent;
            chatListItem5.Text = "最近联系人";
            chatListItem5.TwinkleSubItemNumber = 0;
            this.chatListBox_recent.Items.AddRange(new CCWin.SkinControl.ChatListItem[] {
            chatListItem5});
            this.chatListBox_recent.ListHadOpenGroup = null;
            this.chatListBox_recent.ListSubItemMenu = this.skinContextMenuStrip_Group;
            this.chatListBox_recent.Location = new System.Drawing.Point(1, 136);
            this.chatListBox_recent.Margin = new System.Windows.Forms.Padding(0);
            this.chatListBox_recent.Name = "chatListBox_recent";
            this.chatListBox_recent.SelectSubItem = null;
            this.chatListBox_recent.Size = new System.Drawing.Size(278, 477);
            this.chatListBox_recent.SubItemMenu = null;
            this.chatListBox_recent.TabIndex = 133;
            this.chatListBox_recent.Visible = false;
            this.chatListBox_recent.DoubleClickSubItem += new CCWin.SkinControl.ChatListBox.ChatListEventHandler(this.chatShow_DoubleClickSubItem);
            // 
            // labelSignature
            // 
            this.labelSignature.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSignature.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.Anamorphosis;
            this.labelSignature.BackColor = System.Drawing.Color.Transparent;
            this.labelSignature.BorderColor = System.Drawing.Color.White;
            this.labelSignature.BorderSize = 4;
            this.labelSignature.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelSignature.ForeColor = System.Drawing.Color.Black;
            this.labelSignature.Location = new System.Drawing.Point(76, 58);
            this.labelSignature.Name = "labelSignature";
            this.labelSignature.Size = new System.Drawing.Size(192, 20);
            this.labelSignature.TabIndex = 100;
            this.labelSignature.Text = "退一步海阔天空！";
            // 
            // skinToolStrip1
            // 
            this.skinToolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.skinToolStrip1.Arrow = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.skinToolStrip1.AutoSize = false;
            this.skinToolStrip1.Back = System.Drawing.Color.White;
            this.skinToolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.skinToolStrip1.BackRadius = 4;
            this.skinToolStrip1.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.skinToolStrip1.Base = System.Drawing.Color.Transparent;
            this.skinToolStrip1.BaseFore = System.Drawing.Color.Black;
            this.skinToolStrip1.BaseForeAnamorphosis = true;
            this.skinToolStrip1.BaseForeAnamorphosisBorder = 4;
            this.skinToolStrip1.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.skinToolStrip1.BaseHoverFore = System.Drawing.Color.Black;
            this.skinToolStrip1.BaseItemAnamorphosis = true;
            this.skinToolStrip1.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.skinToolStrip1.BaseItemBorderShow = true;
            this.skinToolStrip1.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("skinToolStrip1.BaseItemDown")));
            this.skinToolStrip1.BaseItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.skinToolStrip1.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("skinToolStrip1.BaseItemMouse")));
            this.skinToolStrip1.BaseItemPressed = System.Drawing.Color.Transparent;
            this.skinToolStrip1.BaseItemRadius = 2;
            this.skinToolStrip1.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip1.BaseItemSplitter = System.Drawing.Color.Transparent;
            this.skinToolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.skinToolStrip1.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinToolStrip1.Fore = System.Drawing.Color.Black;
            this.skinToolStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 4, 2);
            this.skinToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.skinToolStrip1.HoverFore = System.Drawing.Color.White;
            this.skinToolStrip1.ItemAnamorphosis = false;
            this.skinToolStrip1.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.ItemBorderShow = false;
            this.skinToolStrip1.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.ItemRadius = 3;
            this.skinToolStrip1.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.None;
            this.skinToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolstripButton_mainMenu,
            this.toolStripButton1,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripButton19,
            this.toolStripButton2});
            this.skinToolStrip1.Location = new System.Drawing.Point(1, 613);
            this.skinToolStrip1.Name = "skinToolStrip1";
            this.skinToolStrip1.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip1.Size = new System.Drawing.Size(330, 24);
            this.skinToolStrip1.TabIndex = 123;
            this.skinToolStrip1.Text = "skinToolStrip2";
            this.skinToolStrip1.TitleAnamorphosis = false;
            this.skinToolStrip1.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinToolStrip1.TitleRadius = 4;
            this.skinToolStrip1.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolstripButton_mainMenu
            // 
            this.toolstripButton_mainMenu.AutoSize = false;
            this.toolstripButton_mainMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolstripButton_mainMenu.Image = global::GG.Properties.Resources.menu_btn_normal;
            this.toolstripButton_mainMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolstripButton_mainMenu.Margin = new System.Windows.Forms.Padding(4, 1, 0, 2);
            this.toolstripButton_mainMenu.Name = "toolstripButton_mainMenu";
            this.toolstripButton_mainMenu.Size = new System.Drawing.Size(24, 24);
            this.toolstripButton_mainMenu.Text = "toolStripButton4";
            this.toolstripButton_mainMenu.ToolTipText = "主菜单";
            this.toolstripButton_mainMenu.Click += new System.EventHandler(this.toolQQMenu_Click);
            this.toolstripButton_mainMenu.MouseEnter += new System.EventHandler(this.toolstripButton_mainMenu_MouseEnter);
            this.toolstripButton_mainMenu.MouseLeave += new System.EventHandler(this.toolstripButton_mainMenu_MouseLeave);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Margin = new System.Windows.Forms.Padding(4, 1, 0, 2);
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton1.Text = "toolStripButton2";
            this.toolStripButton1.ToolTipText = "打开系统设置";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::GG.Properties.Resources.app_icon_16;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 21);
            this.toolStripButton3.Text = "我的网盘";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 21);
            this.toolStripButton4.Text = "添加好友";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton19
            // 
            this.toolStripButton19.AutoSize = false;
            this.toolStripButton19.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton19.Image")));
            this.toolStripButton19.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.toolStripButton19.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton19.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton19.Name = "toolStripButton19";
            this.toolStripButton19.Size = new System.Drawing.Size(54, 24);
            this.toolStripButton19.Text = "加群";
            this.toolStripButton19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripButton19.ToolTipText = "加群";
            this.toolStripButton19.Click += new System.EventHandler(this.toolStripButton19_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.AutoSize = false;
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Margin = new System.Windows.Forms.Padding(4, 1, 0, 2);
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton2.Text = "toolStripButton3";
            this.toolStripButton2.ToolTipText = "打开消息管理器";
            // 
            // labelName
            // 
            this.labelName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelName.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.Anamorphosis;
            this.labelName.BackColor = System.Drawing.Color.Transparent;
            this.labelName.BorderColor = System.Drawing.Color.White;
            this.labelName.BorderSize = 4;
            this.labelName.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelName.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.labelName.ForeColor = System.Drawing.Color.Black;
            this.labelName.Location = new System.Drawing.Point(99, 35);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(130, 23);
            this.labelName.TabIndex = 99;
            this.labelName.Text = "David";
            // 
            // skinButton_State
            // 
            this.skinButton_State.BackColor = System.Drawing.Color.Transparent;
            this.skinButton_State.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.skinButton_State.BackRectangle = new System.Drawing.Rectangle(4, 4, 4, 4);
            this.skinButton_State.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.skinButton_State.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton_State.DownBack = global::GG.Properties.Resources.allbtn_down;
            this.skinButton_State.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.skinButton_State.Image = global::GG.Properties.Resources.imonline__2_;
            this.skinButton_State.ImageWidth = 12;
            this.skinButton_State.Location = new System.Drawing.Point(73, 35);
            this.skinButton_State.Margin = new System.Windows.Forms.Padding(0);
            this.skinButton_State.MouseBack = global::GG.Properties.Resources.allbtn_highlight;
            this.skinButton_State.Name = "skinButton_State";
            this.skinButton_State.NormlBack = null;
            this.skinButton_State.Palace = true;
            this.skinButton_State.Size = new System.Drawing.Size(23, 23);
            this.skinButton_State.TabIndex = 128;
            this.skinButton_State.Tag = "1";
            this.skinButton_State.UseVisualStyleBackColor = false;
            this.skinButton_State.Click += new System.EventHandler(this.skinButton_State_Click);
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.AutoSize = false;
            this.toolStripMenuItem23.Image = global::GG.Properties.Resources.imonline__2_;
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            this.toolStripMenuItem23.Size = new System.Drawing.Size(105, 22);
            this.toolStripMenuItem23.Tag = "1";
            this.toolStripMenuItem23.Text = "我在线上";
            this.toolStripMenuItem23.ToolTipText = "表示希望好友看到您在线。\r\n声音：开启\r\n消息提醒框：开启\r\n会话消息：任务栏头像闪动\r\n";
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.AutoSize = false;
            this.toolStripMenuItem24.Image = global::GG.Properties.Resources.Qme__2_;
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            this.toolStripMenuItem24.Size = new System.Drawing.Size(105, 22);
            this.toolStripMenuItem24.Tag = "2";
            this.toolStripMenuItem24.Text = "Q我把";
            this.toolStripMenuItem24.ToolTipText = "表示希望好友主动联系您。\r\n声音：开启\r\n消息提醒框：开启\r\n会话消息：自动弹出会话窗口\r\n";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(121, 6);
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.AutoSize = false;
            this.toolStripMenuItem25.Image = global::GG.Properties.Resources.away__2_;
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            this.toolStripMenuItem25.Size = new System.Drawing.Size(105, 22);
            this.toolStripMenuItem25.Tag = "3";
            this.toolStripMenuItem25.Text = "离开";
            this.toolStripMenuItem25.ToolTipText = "表示离开，暂无法处理消息。\r\n声音：开启\r\n消息提醒框：开启\r\n会话消息：任务栏头像闪动\r\n";
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.AutoSize = false;
            this.toolStripMenuItem26.Image = global::GG.Properties.Resources.busy__2_;
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            this.toolStripMenuItem26.Size = new System.Drawing.Size(105, 22);
            this.toolStripMenuItem26.Tag = "4";
            this.toolStripMenuItem26.Text = "忙碌";
            this.toolStripMenuItem26.ToolTipText = "表示忙碌，不会及时处理消息。\r\n声音：开启\r\n消息提醒框：开启\r\n会话消息：任务栏显示气泡\r\n";
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.AutoSize = false;
            this.toolStripMenuItem27.Image = global::GG.Properties.Resources.mute__2_;
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(105, 22);
            this.toolStripMenuItem27.Tag = "5";
            this.toolStripMenuItem27.Text = "请勿打扰";
            this.toolStripMenuItem27.ToolTipText = "表示不想被打扰。\r\n声音：关闭\r\n消息提醒框：关闭\r\n会话消息：任务栏显示气泡\r\n\r\n";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(121, 6);
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.AutoSize = false;
            this.toolStripMenuItem28.Image = global::GG.Properties.Resources.invisible__2_;
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(105, 22);
            this.toolStripMenuItem28.Tag = "6";
            this.toolStripMenuItem28.Text = "隐身";
            this.toolStripMenuItem28.ToolTipText = "表示好友看到您是离线的。\r\n声音：开启\r\n消息提醒框：开启\r\n会话消息：任务栏头像闪动\r\n";
            // 
            // skinContextMenuStrip_State
            // 
            this.skinContextMenuStrip_State.Arrow = System.Drawing.Color.Black;
            this.skinContextMenuStrip_State.Back = System.Drawing.Color.White;
            this.skinContextMenuStrip_State.BackRadius = 4;
            this.skinContextMenuStrip_State.Base = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(200)))), ((int)(((byte)(254)))));
            this.skinContextMenuStrip_State.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinContextMenuStrip_State.Fore = System.Drawing.Color.Black;
            this.skinContextMenuStrip_State.HoverFore = System.Drawing.Color.White;
            this.skinContextMenuStrip_State.ItemAnamorphosis = false;
            this.skinContextMenuStrip_State.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinContextMenuStrip_State.ItemBorderShow = false;
            this.skinContextMenuStrip_State.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinContextMenuStrip_State.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinContextMenuStrip_State.ItemRadius = 4;
            this.skinContextMenuStrip_State.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.None;
            this.skinContextMenuStrip_State.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem20,
            this.toolStripMenuItem30,
            this.toolStripMenuItem31,
            this.toolStripMenuItem32,
            this.toolStripMenuItem33,
            this.toolStripSeparator6,
            this.toolStripMenuItem39,
            this.toolStripMenuItem40});
            this.skinContextMenuStrip_State.ItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinContextMenuStrip_State.Name = "MenuState";
            this.skinContextMenuStrip_State.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinContextMenuStrip_State.Size = new System.Drawing.Size(134, 164);
            this.skinContextMenuStrip_State.TitleAnamorphosis = false;
            this.skinContextMenuStrip_State.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinContextMenuStrip_State.TitleRadius = 4;
            this.skinContextMenuStrip_State.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Image = global::GG.Properties.Resources.imonline__2_;
            this.toolStripMenuItem20.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem20.Tag = "2";
            this.toolStripMenuItem20.Text = "我在线上";
            this.toolStripMenuItem20.ToolTipText = "表示希望好友看到您在线。\r\n声音：开启\r\n消息提醒框：开启\r\n会话消息：任务栏头像闪动\r\n";
            this.toolStripMenuItem20.Click += new System.EventHandler(this.Item_Click);
            // 
            // toolStripMenuItem30
            // 
            this.toolStripMenuItem30.Image = global::GG.Properties.Resources.away__2_;
            this.toolStripMenuItem30.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem30.Name = "toolStripMenuItem30";
            this.toolStripMenuItem30.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem30.Tag = "3";
            this.toolStripMenuItem30.Text = "离开";
            this.toolStripMenuItem30.ToolTipText = "表示离开，暂无法处理消息。\r\n声音：开启\r\n消息提醒框：开启\r\n会话消息：任务栏头像闪动\r\n";
            this.toolStripMenuItem30.Click += new System.EventHandler(this.Item_Click);
            // 
            // toolStripMenuItem31
            // 
            this.toolStripMenuItem31.Image = global::GG.Properties.Resources.busy__2_;
            this.toolStripMenuItem31.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem31.Name = "toolStripMenuItem31";
            this.toolStripMenuItem31.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem31.Tag = "4";
            this.toolStripMenuItem31.Text = "忙碌";
            this.toolStripMenuItem31.ToolTipText = "表示忙碌，不会及时处理消息。\r\n声音：开启\r\n消息提醒框：开启\r\n会话消息：任务栏显示气泡\r\n";
            this.toolStripMenuItem31.Click += new System.EventHandler(this.Item_Click);
            // 
            // toolStripMenuItem32
            // 
            this.toolStripMenuItem32.Image = global::GG.Properties.Resources.mute__2_;
            this.toolStripMenuItem32.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem32.Name = "toolStripMenuItem32";
            this.toolStripMenuItem32.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem32.Tag = "5";
            this.toolStripMenuItem32.Text = "请勿打扰";
            this.toolStripMenuItem32.ToolTipText = "表示不想被打扰。\r\n声音：关闭\r\n消息提醒框：关闭\r\n会话消息：任务栏显示气泡\r\n\r\n";
            this.toolStripMenuItem32.Click += new System.EventHandler(this.Item_Click);
            // 
            // toolStripMenuItem33
            // 
            this.toolStripMenuItem33.Image = global::GG.Properties.Resources.invisible__2_;
            this.toolStripMenuItem33.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem33.Name = "toolStripMenuItem33";
            this.toolStripMenuItem33.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem33.Tag = "6";
            this.toolStripMenuItem33.Text = "隐身";
            this.toolStripMenuItem33.ToolTipText = "表示好友看到您是离线的。\r\n声音：开启\r\n消息提醒框：开启\r\n会话消息：任务栏头像闪动\r\n";
            this.toolStripMenuItem33.Click += new System.EventHandler(this.Item_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(130, 6);
            // 
            // toolStripMenuItem39
            // 
            this.toolStripMenuItem39.Name = "toolStripMenuItem39";
            this.toolStripMenuItem39.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem39.Text = "系统设置...";
            // 
            // toolStripMenuItem40
            // 
            this.toolStripMenuItem40.Name = "toolStripMenuItem40";
            this.toolStripMenuItem40.Size = new System.Drawing.Size(133, 22);
            this.toolStripMenuItem40.Text = "我的资料...";
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "icon_contacts_selected.png");
            this.imageList.Images.SetKeyName(1, "icon_group_selected.png");
            this.imageList.Images.SetKeyName(2, "icon_last_selected.png");
            // 
            // toolStripSplitButton4
            // 
            this.toolStripSplitButton4.AutoSize = false;
            this.toolStripSplitButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton4.DropDownButtonWidth = 20;
            this.toolStripSplitButton4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加好友ToolStripMenuItem});
            this.toolStripSplitButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton4.Image")));
            this.toolStripSplitButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripSplitButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton4.Name = "toolStripSplitButton4";
            this.toolStripSplitButton4.Size = new System.Drawing.Size(70, 35);
            this.toolStripSplitButton4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripSplitButton4.ToolTipText = "联系人";
            this.toolStripSplitButton4.ButtonClick += new System.EventHandler(this.toolStripSplitButton4_ButtonClick);
            // 
            // 添加好友ToolStripMenuItem
            // 
            this.添加好友ToolStripMenuItem.Name = "添加好友ToolStripMenuItem";
            this.添加好友ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.添加好友ToolStripMenuItem.Text = "添加好友";
            this.添加好友ToolStripMenuItem.Click += new System.EventHandler(this.添加好友ToolStripMenuItem_Click);
            // 
            // toolStripSplitButton3
            // 
            this.toolStripSplitButton3.AutoSize = false;
            this.toolStripSplitButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton3.DropDownButtonWidth = 20;
            this.toolStripSplitButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.加入群ToolStripMenuItem,
            this.创建群ToolStripMenuItem});
            this.toolStripSplitButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton3.Image")));
            this.toolStripSplitButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton3.Name = "toolStripSplitButton3";
            this.toolStripSplitButton3.Size = new System.Drawing.Size(70, 35);
            this.toolStripSplitButton3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripSplitButton3.ToolTipText = "GG群";
            this.toolStripSplitButton3.ButtonClick += new System.EventHandler(this.toolStripSplitButton3_ButtonClick);
            // 
            // 加入群ToolStripMenuItem
            // 
            this.加入群ToolStripMenuItem.Name = "加入群ToolStripMenuItem";
            this.加入群ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.加入群ToolStripMenuItem.Text = "加入群";
            this.加入群ToolStripMenuItem.Click += new System.EventHandler(this.加入群ToolStripMenuItem_Click);
            // 
            // 创建群ToolStripMenuItem
            // 
            this.创建群ToolStripMenuItem.Name = "创建群ToolStripMenuItem";
            this.创建群ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.创建群ToolStripMenuItem.Text = "创建群";
            this.创建群ToolStripMenuItem.Click += new System.EventHandler(this.创建群ToolStripMenuItem_Click);
            // 
            // toolStripSplitButton2
            // 
            this.toolStripSplitButton2.AutoSize = false;
            this.toolStripSplitButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton2.DropDownButtonWidth = 20;
            this.toolStripSplitButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.清空会话列表ToolStripMenuItem});
            this.toolStripSplitButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton2.Image")));
            this.toolStripSplitButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton2.Name = "toolStripSplitButton2";
            this.toolStripSplitButton2.Size = new System.Drawing.Size(70, 35);
            this.toolStripSplitButton2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripSplitButton2.ToolTipText = "会话";
            this.toolStripSplitButton2.ButtonClick += new System.EventHandler(this.toolStripSplitButton2_ButtonClick);
            // 
            // 清空会话列表ToolStripMenuItem
            // 
            this.清空会话列表ToolStripMenuItem.Name = "清空会话列表ToolStripMenuItem";
            this.清空会话列表ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.清空会话列表ToolStripMenuItem.Text = "清空会话列表";
            this.清空会话列表ToolStripMenuItem.Click += new System.EventHandler(this.清空会话列表ToolStripMenuItem_Click);
            // 
            // skinToolStrip3
            // 
            this.skinToolStrip3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.skinToolStrip3.Arrow = System.Drawing.Color.White;
            this.skinToolStrip3.AutoSize = false;
            this.skinToolStrip3.Back = System.Drawing.Color.White;
            this.skinToolStrip3.BackColor = System.Drawing.Color.Transparent;
            this.skinToolStrip3.BackRadius = 4;
            this.skinToolStrip3.BackRectangle = new System.Drawing.Rectangle(2, 2, 2, 2);
            this.skinToolStrip3.Base = System.Drawing.Color.Transparent;
            this.skinToolStrip3.BaseFore = System.Drawing.Color.Black;
            this.skinToolStrip3.BaseForeAnamorphosis = false;
            this.skinToolStrip3.BaseForeAnamorphosisBorder = 4;
            this.skinToolStrip3.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.skinToolStrip3.BaseHoverFore = System.Drawing.Color.Black;
            this.skinToolStrip3.BaseItemAnamorphosis = true;
            this.skinToolStrip3.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.skinToolStrip3.BaseItemBorderShow = false;
            this.skinToolStrip3.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("skinToolStrip3.BaseItemDown")));
            this.skinToolStrip3.BaseItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.skinToolStrip3.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("skinToolStrip3.BaseItemMouse")));
            this.skinToolStrip3.BaseItemPressed = System.Drawing.Color.Transparent;
            this.skinToolStrip3.BaseItemRadius = 2;
            this.skinToolStrip3.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.None;
            this.skinToolStrip3.BaseItemSplitter = System.Drawing.Color.Transparent;
            this.skinToolStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.skinToolStrip3.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinToolStrip3.Fore = System.Drawing.Color.Black;
            this.skinToolStrip3.GripMargin = new System.Windows.Forms.Padding(2, 2, 4, 2);
            this.skinToolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.skinToolStrip3.HoverFore = System.Drawing.Color.White;
            this.skinToolStrip3.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.skinToolStrip3.ItemAnamorphosis = false;
            this.skinToolStrip3.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip3.ItemBorderShow = false;
            this.skinToolStrip3.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip3.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip3.ItemRadius = 3;
            this.skinToolStrip3.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.None;
            this.skinToolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton4,
            this.toolStripSplitButton3,
            this.toolStripSplitButton2});
            this.skinToolStrip3.Location = new System.Drawing.Point(4, 102);
            this.skinToolStrip3.Name = "skinToolStrip3";
            this.skinToolStrip3.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip3.Size = new System.Drawing.Size(275, 34);
            this.skinToolStrip3.TabIndex = 127;
            this.skinToolStrip3.Text = "skinToolStrip3";
            this.skinToolStrip3.TitleAnamorphosis = false;
            this.skinToolStrip3.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinToolStrip3.TitleRadius = 4;
            this.skinToolStrip3.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // skinPanel_HeadImage
            // 
            this.skinPanel_HeadImage.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel_HeadImage.BackgroundImage = global::GG.Properties.Resources._1_100;
            this.skinPanel_HeadImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.skinPanel_HeadImage.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel_HeadImage.DownBack = null;
            this.skinPanel_HeadImage.Location = new System.Drawing.Point(1, 1);
            this.skinPanel_HeadImage.MouseBack = null;
            this.skinPanel_HeadImage.Name = "skinPanel_HeadImage";
            this.skinPanel_HeadImage.NormlBack = null;
            this.skinPanel_HeadImage.Radius = 4;
            this.skinPanel_HeadImage.Size = new System.Drawing.Size(64, 64);
            this.skinPanel_HeadImage.TabIndex = 129;
            // 
            // skinLabel1
            // 
            this.skinLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.skinLabel1.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.Anamorphosis;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.BorderSize = 4;
            this.skinLabel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.skinLabel1.ForeColor = System.Drawing.Color.Black;
            this.skinLabel1.Location = new System.Drawing.Point(6, 2);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(90, 23);
            this.skinLabel1.TabIndex = 99;
            this.skinLabel1.Text = "GG 2013";
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.skinContextMenuStrip_mainMenu;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "GG";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notyfyIcon1_MouseDoubleClick);
            // 
            // skinContextMenuStrip_mainMenu
            // 
            this.skinContextMenuStrip_mainMenu.Arrow = System.Drawing.Color.Black;
            this.skinContextMenuStrip_mainMenu.Back = System.Drawing.Color.White;
            this.skinContextMenuStrip_mainMenu.BackRadius = 4;
            this.skinContextMenuStrip_mainMenu.Base = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(200)))), ((int)(((byte)(254)))));
            this.skinContextMenuStrip_mainMenu.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinContextMenuStrip_mainMenu.Fore = System.Drawing.Color.Black;
            this.skinContextMenuStrip_mainMenu.HoverFore = System.Drawing.Color.White;
            this.skinContextMenuStrip_mainMenu.ImageScalingSize = new System.Drawing.Size(11, 11);
            this.skinContextMenuStrip_mainMenu.ItemAnamorphosis = false;
            this.skinContextMenuStrip_mainMenu.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(136)))), ((int)(((byte)(200)))));
            this.skinContextMenuStrip_mainMenu.ItemBorderShow = false;
            this.skinContextMenuStrip_mainMenu.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(136)))), ((int)(((byte)(200)))));
            this.skinContextMenuStrip_mainMenu.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(136)))), ((int)(((byte)(200)))));
            this.skinContextMenuStrip_mainMenu.ItemRadius = 4;
            this.skinContextMenuStrip_mainMenu.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.None;
            this.skinContextMenuStrip_mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.修改密码ToolStripMenuItem,
            this.toolQQShow,
            this.toolStripMenuItem3,
            this.toolExit});
            this.skinContextMenuStrip_mainMenu.ItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinContextMenuStrip_mainMenu.Name = "MenuState";
            this.skinContextMenuStrip_mainMenu.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinContextMenuStrip_mainMenu.Size = new System.Drawing.Size(137, 76);
            this.skinContextMenuStrip_mainMenu.TitleAnamorphosis = false;
            this.skinContextMenuStrip_mainMenu.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinContextMenuStrip_mainMenu.TitleRadius = 4;
            this.skinContextMenuStrip_mainMenu.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // 修改密码ToolStripMenuItem
            // 
            this.修改密码ToolStripMenuItem.Name = "修改密码ToolStripMenuItem";
            this.修改密码ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.修改密码ToolStripMenuItem.Text = "修改密码";
            this.修改密码ToolStripMenuItem.Click += new System.EventHandler(this.修改密码ToolStripMenuItem_Click);
            // 
            // toolQQShow
            // 
            this.toolQQShow.Name = "toolQQShow";
            this.toolQQShow.Size = new System.Drawing.Size(136, 22);
            this.toolQQShow.Text = "打开主面板";
            this.toolQQShow.Click += new System.EventHandler(this.toolQQShow_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(133, 6);
            // 
            // toolExit
            // 
            this.toolExit.Name = "toolExit";
            this.toolExit.Size = new System.Drawing.Size(136, 22);
            this.toolExit.Text = "退出";
            this.toolExit.Click += new System.EventHandler(this.toolExit_Click);
            // 
            // pnlTx
            // 
            this.pnlTx.BackColor = System.Drawing.Color.Transparent;
            this.pnlTx.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTx.BackgroundImage")));
            this.pnlTx.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTx.Controls.Add(this.skinPanel_HeadImage);
            this.pnlTx.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.pnlTx.DownBack = null;
            this.pnlTx.Location = new System.Drawing.Point(6, 35);
            this.pnlTx.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTx.MouseBack = null;
            this.pnlTx.Name = "pnlTx";
            this.pnlTx.NormlBack = null;
            this.pnlTx.Size = new System.Drawing.Size(67, 67);
            this.pnlTx.TabIndex = 131;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "normal_group_40.png");
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Back = ((System.Drawing.Image)(resources.GetObject("$this.Back")));
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.BackLayout = false;
            this.BorderPalace = global::GG.Properties.Resources.BackPalace;
            this.ClientSize = new System.Drawing.Size(281, 640);
            this.CloseBoxSize = new System.Drawing.Size(39, 20);
            this.CloseDownBack = global::GG.Properties.Resources.btn_close_down;
            this.CloseMouseBack = global::GG.Properties.Resources.btn_close_highlight;
            this.CloseNormlBack = global::GG.Properties.Resources.btn_close_disable;
            this.ControlBoxOffset = new System.Drawing.Point(0, -1);
            this.Controls.Add(this.chatListBox);
            this.Controls.Add(this.chatListBox_group);
            this.Controls.Add(this.pnlTx);
            this.Controls.Add(this.labelSignature);
            this.Controls.Add(this.skinButton_State);
            this.Controls.Add(this.skinToolStrip3);
            this.Controls.Add(this.skinToolStrip1);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.chatListBox_recent);
            this.EffectCaption = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaxDownBack = global::GG.Properties.Resources.btn_max_down;
            this.MaximizeBox = false;
            this.MaxMouseBack = global::GG.Properties.Resources.btn_max_highlight;
            this.MaxNormlBack = global::GG.Properties.Resources.btn_max_normal;
            this.MaxSize = new System.Drawing.Size(28, 20);
            this.MiniDownBack = global::GG.Properties.Resources.btn_mini_down;
            this.MiniMouseBack = global::GG.Properties.Resources.btn_mini_highlight;
            this.MinimumSize = new System.Drawing.Size(281, 640);
            this.MiniNormlBack = global::GG.Properties.Resources.btn_mini_normal;
            this.MiniSize = new System.Drawing.Size(28, 20);
            this.Name = "MainForm";
            this.RestoreDownBack = global::GG.Properties.Resources.btn_restore_down;
            this.RestoreMouseBack = global::GG.Properties.Resources.btn_restore_highlight;
            this.RestoreNormlBack = global::GG.Properties.Resources.btn_restore_normal;
            this.Shadow = false;
            this.ShowBorder = false;
            this.ShowDrawIcon = false;
            this.ShowInTaskbar = false;
            this.SkinOpacity = 0.95D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.SysBottomDown = global::GG.Properties.Resources.btn_Skin_down;
            this.SysBottomMouse = global::GG.Properties.Resources.btn_Skin_highlight;
            this.SysBottomNorml = global::GG.Properties.Resources.btn_Skin_normal;
            this.SysBottomToolTip = "更改外观";
            this.SysBottomVisibale = true;
            this.Text = "GG 2013";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.skinContextMenuStrip_user.ResumeLayout(false);
            this.skinContextMenuStrip_Group.ResumeLayout(false);
            this.skinToolStrip1.ResumeLayout(false);
            this.skinToolStrip1.PerformLayout();
            this.skinContextMenuStrip_State.ResumeLayout(false);
            this.skinToolStrip3.ResumeLayout(false);
            this.skinToolStrip3.PerformLayout();
            this.skinContextMenuStrip_mainMenu.ResumeLayout(false);
            this.pnlTx.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinLabel labelSignature;
        private System.Windows.Forms.ToolTip toolTip1;
        private CCWin.SkinControl.SkinToolStrip skinToolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private CCWin.SkinControl.SkinLabel labelName;
        private System.Windows.Forms.ToolStripButton toolstripButton_mainMenu;
        private CCWin.SkinControl.SkinButton skinButton_State;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private CCWin.SkinControl.SkinContextMenuStrip skinContextMenuStrip_State;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem30;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem31;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem32;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem33;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem39;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem40;
        private CCWin.SkinControl.SkinContextMenuStrip skinContextMenuStrip_user;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem51;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem60;
        private System.Windows.Forms.ToolStripMenuItem 移动联系人至ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 我的好友ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 自己ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 黑名单ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除好友ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 修改备注名ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem53;
        private System.Windows.Forms.ToolStripMenuItem 消息记录ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 查看资料ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 查看本地消息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 查看漫游消息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 查看上传消息ToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList;
        private CCWin.SkinControl.ChatListBox chatListBox;
        private System.Windows.Forms.ToolStripButton toolStripButton19;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton4;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton3;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton2;
        private CCWin.SkinControl.SkinToolStrip skinToolStrip3;
        private CCWin.SkinControl.SkinPanel skinPanel_HeadImage;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        public System.Windows.Forms.NotifyIcon notifyIcon;
        private CCWin.SkinControl.SkinContextMenuStrip skinContextMenuStrip_mainMenu;
        private System.Windows.Forms.ToolStripMenuItem toolQQShow;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolExit;
        private CCWin.SkinControl.SkinPanel pnlTx;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private CCWin.SkinControl.ChatListBox chatListBox_group;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripMenuItem 加入群ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 创建群ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 添加好友ToolStripMenuItem;
        private CCWin.SkinControl.SkinContextMenuStrip skinContextMenuStrip_Group;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private CCWin.SkinControl.ChatListBox chatListBox_recent;
        private System.Windows.Forms.ToolStripMenuItem 清空会话列表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 修改密码ToolStripMenuItem;
    }
}

