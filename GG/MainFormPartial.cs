﻿using System;
using System.Collections.Generic;
using System.Text;
using GG.Core;
using CCWin.Win32;
using System.Windows.Forms;
using CCWin.SkinControl;
using ESBasic;
using ESPlus.Serialization;
using OMCS.Passive.MicroMessages;

namespace GG
{
    public partial class MainForm : ESPlus.Application.CustomizeInfo.IIntegratedCustomizeHandler
    {
        #region HandleInformation
		public void HandleInformation(string sourceUserID, int informationType, byte[] info)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new CbGeneric<string, int, byte[]>(this.HandleInformation), sourceUserID, informationType, info);
            }
            else
            {
                if (informationType == InformationTypes.Chat)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    string rtfMsg = System.Text.Encoding.UTF8.GetString(info);
                    form.OnReceivedMsg(rtfMsg);
                    return;
                }

                if (informationType == InformationTypes.OfflineMessage)
                {
                    OfflineMessage msg = CompactPropertySerializer.Default.Deserialize<OfflineMessage>(info, 0);
                    if (msg.InformationType == InformationTypes.Chat)
                    {
                        ChatForm form = this.GetChatForm(msg.SourceUserID);
                        string rtfMsg = System.Text.Encoding.UTF8.GetString(msg.Information);
                        form.OnReceivedOfflineMessage(msg.Time, rtfMsg);
                        return;
                    }

                    if (msg.InformationType == InformationTypes.OfflineAudioMessage) //V3.6
                    {
                        MicroMessage microMessage = CompactPropertySerializer.Default.Deserialize<MicroMessage>(msg.Information, 0);
                        ChatForm form = this.GetChatForm(msg.SourceUserID);
                        form.OnMicroMessageReceived(microMessage ,true);
                        return;
                    }

                    return;
                }

                if (informationType == InformationTypes.OfflineFileResultNotify)
                {
                    OfflineFileResultNotifyContract contract = CompactPropertySerializer.Default.Deserialize<OfflineFileResultNotifyContract>(info, 0);
                    ChatForm form = this.GetChatForm(contract.AccepterID);
                    form.OnReceivedOfflineFileResultNotify(contract.FileName, contract.Accept);
                    return;
                }

                if (informationType == InformationTypes.Vibration)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnViberation();
                    return;
                }

                if (informationType == InformationTypes.VideoRequest)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnVideoRequestReceived();
                    return;
                }

                if (informationType == InformationTypes.AgreeVideo)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnVideoAnswerReceived(true);
                    return;
                }

                if (informationType == InformationTypes.RejectVideo)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnVideoAnswerReceived(false);
                    return;
                }

                if (informationType == InformationTypes.HungUpVideo)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnVideoHungUpReceived();
                    return;
                }

                if (informationType == InformationTypes.DiskRequest)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnDiskRequestReceived();
                    return;
                }

                if (informationType == InformationTypes.AgreeDisk)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnDiskAnswerReceived(true);
                    return;
                }

                if (informationType == InformationTypes.RejectDisk)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnDiskAnswerReceived(false);
                    return;
                }

                if (informationType == InformationTypes.RemoteHelpRequest)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnRemoteHelpRequestReceived();
                    return;
                }

                if (informationType == InformationTypes.AgreeRemoteHelp)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnRemoteHelpAnswerReceived(true);
                    return;
                }

                if (informationType == InformationTypes.RejectRemoteHelp)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnRemoteHelpAnswerReceived(false);
                    return;
                }

                if (informationType == InformationTypes.CloseRemoteHelp)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnHelperCloseRemoteHelp();
                    return;
                }

                if (informationType == InformationTypes.TerminateRemoteHelp)
                {
                    ChatForm form = this.GetChatForm(sourceUserID);
                    form.OnOwnerTerminateRemoteHelp();
                    return;
                }

                if (informationType == InformationTypes.FriendAddedNotify)
                {
                    string ownerID = System.Text.Encoding.UTF8.GetString(info);
                    this.OnAddedFriend(ownerID);
                    return;
                }

                if (informationType == InformationTypes.FriendRemovedNotify)
                {
                    string friendID = System.Text.Encoding.UTF8.GetString(info);
                    this.OnRemovedFriend(friendID);
                    return;
                }
            }
        } 
	    #endregion

        public byte[] HandleQuery(string sourceUserID, int informationType, byte[] info)
        {
            return null;
        }

        public bool CanHandle(int informationType)
        {
            return InformationTypes.ContainsInformationType(informationType);
        }


        void GroupOutter_BroadcastReceived(string broadcasterID, string groupID, int broadcastType, byte[] content)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new CbGeneric<string, string, int, byte[]>(this.GroupOutter_BroadcastReceived), broadcasterID, groupID, broadcastType, content);
            }
            else
            {
                if (broadcastType == BroadcastTypes.BroadcastChat)
                {
                    GroupChatForm form = this.GetGroupChatForm(groupID);
                    string rtfMsg = System.Text.Encoding.UTF8.GetString(content);
                    form.OnReceivedMsg(broadcasterID, rtfMsg);
                    return;
                }

                if (broadcastType == BroadcastTypes.SomeoneJoinGroup)
                {
                    Group group = this.groupCache.Get(groupID);
                    string userID = System.Text.Encoding.UTF8.GetString(content);
                    if (group == null || group.Members.Contains(userID))
                    {
                        return;
                    }
                    group.Members.Add(userID);

                    ChatListSubItem subItem = this.chatListBox_group.GetSubItemsByNicName(group.ID)[0];
                    subItem.PersonalMsg = string.Format("{0}人", group.Members.Count);
                    
                    GroupChatForm form = this.groupChatFormManager.GetForm(groupID);
                    if (form != null)
                    {                                             
                        form.OnSomeoneJoin(userID);
                    }                   
                    return;
                }

                if (broadcastType == BroadcastTypes.SomeoneQuitGroup)
                {
                    Group group = this.groupCache.Get(groupID);
                    string userID = System.Text.Encoding.UTF8.GetString(content);
                    if (group == null || !group.Members.Contains(userID))
                    {
                        return;
                    }

                    group.Members.Remove(userID);

                    ChatListSubItem subItem = this.chatListBox_group.GetSubItemsByNicName(group.ID)[0];
                    subItem.PersonalMsg = string.Format("{0}人", group.Members.Count);
                    
                    GroupChatForm form = this.groupChatFormManager.GetForm(groupID);
                    if (form != null)                    
                    {                       
                        form.OnSomeoneQuit(userID);
                    }
                    return;
                }
            }
        }
    }
}
