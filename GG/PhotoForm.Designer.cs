﻿namespace GG
{
    partial class PhotoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhotoForm));
            this.btnPhoto = new CCWin.SkinControl.SkinButton();
            this.photoPanel1 = new OMCS.Tools.PhotoPanel();
            this.skinLabel_msg = new CCWin.SkinControl.SkinLabel();
            this.SuspendLayout();
            // 
            // btnPhoto
            // 
            this.btnPhoto.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPhoto.BackColor = System.Drawing.Color.Transparent;
            this.btnPhoto.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.btnPhoto.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btnPhoto.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPhoto.DownBack = ((System.Drawing.Image)(resources.GetObject("btnPhoto.DownBack")));
            this.btnPhoto.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.btnPhoto.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnPhoto.Location = new System.Drawing.Point(147, 305);
            this.btnPhoto.MouseBack = ((System.Drawing.Image)(resources.GetObject("btnPhoto.MouseBack")));
            this.btnPhoto.Name = "btnPhoto";
            this.btnPhoto.NormlBack = ((System.Drawing.Image)(resources.GetObject("btnPhoto.NormlBack")));
            this.btnPhoto.Size = new System.Drawing.Size(69, 24);
            this.btnPhoto.TabIndex = 133;
            this.btnPhoto.Text = "拍照";
            this.btnPhoto.UseVisualStyleBackColor = false;
            this.btnPhoto.Visible = false;
            this.btnPhoto.Click += new System.EventHandler(this.btnPhoto_Click);
            // 
            // photoPanel1
            // 
            this.photoPanel1.CameraIndex = 0;
            this.photoPanel1.ImageSize = new System.Drawing.Size(320, 240);
            this.photoPanel1.Location = new System.Drawing.Point(29, 49);
            this.photoPanel1.Name = "photoPanel1";
            this.photoPanel1.Size = new System.Drawing.Size(320, 240);
            this.photoPanel1.TabIndex = 134;
            // 
            // skinLabel_msg
            // 
            this.skinLabel_msg.AutoSize = true;
            this.skinLabel_msg.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel_msg.BorderColor = System.Drawing.Color.White;
            this.skinLabel_msg.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel_msg.Location = new System.Drawing.Point(26, 305);
            this.skinLabel_msg.Name = "skinLabel_msg";
            this.skinLabel_msg.Size = new System.Drawing.Size(77, 17);
            this.skinLabel_msg.TabIndex = 135;
            this.skinLabel_msg.Text = "正在启动 . . .";
            // 
            // PhotoForm
            // 
            this.AcceptButton = this.btnPhoto;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Back = ((System.Drawing.Image)(resources.GetObject("$this.Back")));
            this.BackLayout = false;
            this.BorderPalace = ((System.Drawing.Image)(resources.GetObject("$this.BorderPalace")));
            this.CanResize = false;
            this.CaptionFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.ClientSize = new System.Drawing.Size(377, 347);
            this.CloseBoxSize = new System.Drawing.Size(39, 20);
            this.CloseDownBack = global::GG.Properties.Resources.btn_close_down;
            this.CloseMouseBack = global::GG.Properties.Resources.btn_close_highlight;
            this.CloseNormlBack = global::GG.Properties.Resources.btn_close_disable;
            this.ControlBoxOffset = new System.Drawing.Point(0, -1);
            this.Controls.Add(this.skinLabel_msg);
            this.Controls.Add(this.photoPanel1);
            this.Controls.Add(this.btnPhoto);
            this.DropBack = false;
            this.EffectWidth = 2;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaxDownBack = global::GG.Properties.Resources.btn_max_down;
            this.MaximizeBox = false;
            this.MaxMouseBack = global::GG.Properties.Resources.btn_max_highlight;
            this.MaxNormlBack = global::GG.Properties.Resources.btn_max_normal;
            this.MaxSize = new System.Drawing.Size(28, 20);
            this.MiniDownBack = global::GG.Properties.Resources.btn_mini_down;
            this.MinimizeBox = false;
            this.MiniMouseBack = global::GG.Properties.Resources.btn_mini_highlight;
            this.MiniNormlBack = global::GG.Properties.Resources.btn_mini_normal;
            this.MiniSize = new System.Drawing.Size(28, 20);
            this.Name = "PhotoForm";
            this.RestoreDownBack = global::GG.Properties.Resources.btn_restore_down;
            this.RestoreMouseBack = global::GG.Properties.Resources.btn_restore_highlight;
            this.RestoreNormlBack = global::GG.Properties.Resources.btn_restore_normal;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "自拍头像";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PhotoForm_FormClosing);
            this.Shown += new System.EventHandler(this.PhotoForm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinButton btnPhoto;
        private OMCS.Tools.PhotoPanel photoPanel1;
        private CCWin.SkinControl.SkinLabel skinLabel_msg;
    }
}