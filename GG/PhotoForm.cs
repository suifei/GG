﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CCWin;
using CCWin.Win32;
using CCWin.Win32.Const;
using System.Diagnostics;
using GG.Core;
using System.Configuration;
using ESBasic.Security;

namespace GG
{
    /// <summary>
    /// 自拍头像。 V3.5
    /// </summary>
    public partial class PhotoForm : CCSkinMain
    {
        private int headImageIndex = 0;

        public PhotoForm()
        {
            InitializeComponent();            
        }

        private void PhotoForm_Shown(object sender, EventArgs e)
        {
            this.photoPanel1.Start();
            this.skinLabel_msg.Visible = false;
            this.btnPhoto.Visible = true;            
        }

        #region HeadImage
        private Bitmap headImage = null;
        public Bitmap HeadImage
        {
            get { return headImage; }
        } 
        #endregion

        private void btnPhoto_Click(object sender, EventArgs e)
        {
            Bitmap bm = this.photoPanel1.GetCurrentImage();

            //将头像转换为正方形
            int delt = bm.Width - bm.Height;
            Bitmap newBm = new Bitmap(100, 100);
            using (Graphics g = Graphics.FromImage(newBm))
            {
                g.DrawImage(bm, new Rectangle(0, 0, 100, 100), new Rectangle(delt / 2, 0, bm.Height, bm.Height), GraphicsUnit.Pixel);
            }

            this.headImage = newBm;
            this.photoPanel1.Stop();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void PhotoForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.photoPanel1.Stop();
        } 
    }
}
