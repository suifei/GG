﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ESPlus.Rapid;
using GGLib.NDisk.Passive;
using ESPlus.Application.CustomizeInfo;

namespace GG
{
    static class Program
    {
        public static string[] NickNameArray = { "Martin", "David", "Tom", "John", "Steven", "Jack", "Jay", "Bruce", "Chris", "Denny", "Jason", "Kevin" };

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                
                ESPlus.GlobalUtil.SetMaxLengthOfUserID(20);                
                OMCS.GlobalUtil.SetMaxLengthOfUserID(20);
                MainForm mainForm = new MainForm();
                IRapidPassiveEngine passiveEngine = RapidEngineFactory.CreatePassiveEngine();

                NDiskPassiveHandler nDiskPassiveHandler = new NDiskPassiveHandler(); //V 2.0
                ComplexCustomizeHandler complexHandler = new ComplexCustomizeHandler(nDiskPassiveHandler, mainForm);//V 2.0
                LoginForm loginForm = new LoginForm(passiveEngine, complexHandler);

                if (loginForm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                nDiskPassiveHandler.Initialize(passiveEngine.FileOutter);
                mainForm.Initialize(passiveEngine, loginForm.LoginStatus, loginForm.StateImage);           
                mainForm.ShowNotifyIcon();
                Application.Run(mainForm);
            }
            catch(Exception ee)
            {
                MessageBox.Show(ee.Message);
                ee = ee;
            }
        }
    }
}
