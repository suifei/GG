﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CCWin;
using CCWin.Win32;
using CCWin.Win32.Const;
using System.Diagnostics;
using GG.Core;
using System.Configuration;
using ESBasic.Security;

namespace GG
{
    public partial class RegisterForm : CCSkinMain
    {
        private Bitmap headImage = null;
        private int headImageIndex = 0;

        public RegisterForm()
        {
            InitializeComponent();

            Random ran = new Random();
            this.headImageIndex = ran.Next(0,10);
            this.pnlImgTx.BackgroundImage = Image.FromFile("head/" + this.headImageIndex.ToString() + ".png"); //根据ID获取头像 
        }

        #region RegisteredUser
        private User registeredUser = null;
        public User RegisteredUser
        {
            get
            {
                return this.registeredUser;
            }
        } 
        #endregion

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://justnow.cnblogs.com");
        }

        private void skinButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;            
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            string userID = this.skinTextBox_id.SkinTxt.Text.Trim();
            if (userID.Length == 0)
            {
                MessageBox.Show("帐号不能为空！");
                return;
            }

            try
            {
                int registerPort = int.Parse(ConfigurationManager.AppSettings["RemotingPort"]);
                IRegisterService registerService = (IRegisterService)Activator.GetObject(typeof(IRegisterService), string.Format("tcp://{0}:{1}/RegisterService", ConfigurationManager.AppSettings["ServerIP"], registerPort)); ;
                User user = new User(userID,SecurityHelper.MD5String(this.skinTextBox_pwd.SkinTxt.Text), this.skinTextBox_nickName.SkinTxt.Text, this.skinTextBox_signature.SkinTxt.Text ,this.headImageIndex);
                if (this.headImage != null) //使用自拍头像
                {
                    user.HeadImage = ESBasic.Helpers.ImageHelper.Convert(this.headImage);
                    user.HeadImageIndex = -1;
                }
               
                RegisterResult result = registerService.Register(user);
                if (result == RegisterResult.Existed)
                {
                    MessageBox.Show("用户帐号已经存在！");                    
                    return;
                }

                if (result == RegisterResult.Error)
                {
                    MessageBox.Show("注册出现错误！");                    
                    return;
                }

                this.registeredUser = user;
                MessageBox.Show("注册成功！接下来将使用此帐号自动登录！");
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception ee)
            {
                MessageBox.Show("注册失败！" + ee.Message);
            }

        }
       
        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.headImageIndex = ( ++this.headImageIndex)%10;
            this.pnlImgTx.BackgroundImage = Image.FromFile("head/" + this.headImageIndex.ToString() + ".png"); //根据ID获取头像 
            this.headImage = null;
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PhotoForm form = new PhotoForm();
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.pnlImgTx.BackgroundImage = form.HeadImage;
                this.headImage = form.HeadImage;                
            }
        }     
    }
}
