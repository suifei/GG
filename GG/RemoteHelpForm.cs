﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CCWin;
using CCWin.Win32;
using CCWin.Win32.Const;
using System.Diagnostics;
using ESBasic;
using OMCS.Passive;
using ESPlus.Application.FileTransfering.Passive;
using GGLib.NDisk.Passive;

namespace GG
{
    public partial class RemoteHelpForm : CCSkinMain
    {
        public event CbGeneric<bool> RemoteHelpEnded; //参数：true - 协助方终止；false - 请求方终止

        private string ownerName = "";
        public RemoteHelpForm(string ownerID, string nickName)
        {
            InitializeComponent();
            this.RemoteHelpEnded += delegate { };

            this.ownerName = nickName;
            this.Text = string.Format("远程桌面 - {0}", this.ownerName);         
            this.Cursor = Cursors.WaitCursor;

            this.desktopConnector1.Visible = false;
            this.desktopConnector1.WatchingOnly = false; //可以操控桌面
            this.desktopConnector1.ConnectEnded += new CbGeneric<ConnectResult>(desktopConnector1_ConnectEnded);
            this.desktopConnector1.Disconnected += new CbGeneric<ConnectorDisconnectedType>(desktopConnector1_Disconnected);

            this.desktopConnector1.BeginConnect(ownerID);
        } 

        void desktopConnector1_Disconnected(ConnectorDisconnectedType disconnectedType)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new CbGeneric<ConnectorDisconnectedType>(this.desktopConnector1_Disconnected), disconnectedType);
            }
            else
            {
                if (disconnectedType == ConnectorDisconnectedType.GuestActiveDisconnect)
                {
                    return;
                }

                MessageBox.Show(string.Format("到{0}的远程桌面连接断开。原因：{1}", this.ownerName, disconnectedType));
                this.Close();
            }
        }

        void desktopConnector1_ConnectEnded(ConnectResult res)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new CbGeneric<ConnectResult>(this.desktopConnector1_ConnectEnded), res);
            }
            else
            {
                this.Cursor = Cursors.Default;
                if (res == ConnectResult.Succeed)
                {
                    this.desktopConnector1.Visible = true;         
                    return;
                }

                MessageBox.Show(string.Format("连接{0}的桌面失败。原因：{1}", this.ownerName, res));
                this.Close();
            }
        }

        private bool ownerCancel = false;
        internal void OwnerTeminateHelp()
        {
            this.ownerCancel = true;
            this.Close();
        }

        private void RemoteHelpForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.desktopConnector1.Connected)
            {
                return;
            }

            this.RemoteHelpEnded(this.ownerCancel);
        }       
    }
}
