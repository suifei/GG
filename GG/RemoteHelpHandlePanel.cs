﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ESBasic;

namespace GG
{
    /// <summary>
    /// 远程桌面的主人控制面板
    /// </summary>
    public partial class RemoteHelpHandlePanel : UserControl
    {
        private System.Windows.Forms.Timer timer;

        /// <summary>
        /// 中止远程协助
        /// </summary>
        public event CbGeneric RemoteHelpTerminated;

        public RemoteHelpHandlePanel()
        {
            InitializeComponent();

            this.skinLabel_time.Visible = false;
            this.skinLabel_msg.Visible = true;
        }

        public bool IsWorking
        {
            get
            {
                return this.timer != null;
            }
        }

        private void skinButtomReject_Click(object sender, EventArgs e)
        {
            if (this.RemoteHelpTerminated != null)
            {
                this.RemoteHelpTerminated();
            }

            this.OnTerminate();
        }

        public void OnAgree()
        {
            this.skinLabel_time.Visible = true;
            this.skinLabel_msg.Visible = false;
            this.timer = new Timer();
            this.timer.Interval = 1000;
            this.timer.Tick += new EventHandler(timer_Tick);
            this.timer.Start();
        }

        private int elapsedSecs = 0;
        void timer_Tick(object sender, EventArgs e)
        {
            ++this.elapsedSecs;
            int mins = this.elapsedSecs / 60;
            int secs = this.elapsedSecs % 60;
            this.skinLabel_time.Text = string.Format("{0}:{1}", mins.ToString("00"), secs.ToString("00"));
        }
        
        public void OnTerminate()
        {
            if (this.timer == null)
            {
                return;
            }
            this.timer.Stop();
            this.timer.Dispose();
            this.skinLabel_time.Text = "00:00";
            this.skinLabel_time.Visible = false;
            this.skinLabel_msg.Visible = true;
            this.timer = null;
            this.elapsedSecs = 0;
        }
    }
}
