﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ESBasic;

namespace GG
{
    public partial class RemoteHelpRequestPanel : UserControl
    {
       
        /// <summary>
        /// 回复远程协助请求
        /// </summary>
        public event CbGeneric<bool> RemoteHelpRequestAnswerd;

        public RemoteHelpRequestPanel()
        {
            InitializeComponent();
        }

        private void skinButtomReject_Click(object sender, EventArgs e)
        {
            if (this.RemoteHelpRequestAnswerd != null)
            {
                this.RemoteHelpRequestAnswerd(false);
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (this.RemoteHelpRequestAnswerd != null)
            {
                this.RemoteHelpRequestAnswerd(true);
            }
        }

        
    }
}
