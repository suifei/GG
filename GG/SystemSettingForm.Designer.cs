﻿namespace GG
{
    partial class SystemSettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemSettingForm));
            this.btnClose = new CCWin.SkinControl.SkinButton();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinRadioButton_hide = new CCWin.SkinControl.SkinRadioButton();
            this.skinRadioButton2 = new CCWin.SkinControl.SkinRadioButton();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.skinComboBox_camera = new CCWin.SkinControl.SkinComboBox();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.skinComboBox_mic = new CCWin.SkinControl.SkinComboBox();
            this.skinCheckBox_autoRun = new CCWin.SkinControl.SkinCheckBox();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.btnClose.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.DownBack = ((System.Drawing.Image)(resources.GetObject("btnClose.DownBack")));
            this.btnClose.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.btnClose.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnClose.Location = new System.Drawing.Point(150, 193);
            this.btnClose.MouseBack = ((System.Drawing.Image)(resources.GetObject("btnClose.MouseBack")));
            this.btnClose.Name = "btnClose";
            this.btnClose.NormlBack = ((System.Drawing.Image)(resources.GetObject("btnClose.NormlBack")));
            this.btnClose.Size = new System.Drawing.Size(69, 24);
            this.btnClose.TabIndex = 133;
            this.btnClose.Text = "确定";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(11, 51);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(92, 17);
            this.skinLabel2.TabIndex = 135;
            this.skinLabel2.Text = "关闭主面板时：";
            // 
            // skinRadioButton_hide
            // 
            this.skinRadioButton_hide.AutoSize = true;
            this.skinRadioButton_hide.BackColor = System.Drawing.Color.Transparent;
            this.skinRadioButton_hide.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinRadioButton_hide.DownBack = null;
            this.skinRadioButton_hide.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinRadioButton_hide.Location = new System.Drawing.Point(109, 49);
            this.skinRadioButton_hide.MouseBack = null;
            this.skinRadioButton_hide.Name = "skinRadioButton_hide";
            this.skinRadioButton_hide.NormlBack = null;
            this.skinRadioButton_hide.SelectedDownBack = null;
            this.skinRadioButton_hide.SelectedMouseBack = null;
            this.skinRadioButton_hide.SelectedNormlBack = null;
            this.skinRadioButton_hide.Size = new System.Drawing.Size(146, 21);
            this.skinRadioButton_hide.TabIndex = 136;
            this.skinRadioButton_hide.Text = "隐藏到任务栏通知区域";
            this.skinRadioButton_hide.UseVisualStyleBackColor = false;
            // 
            // skinRadioButton2
            // 
            this.skinRadioButton2.AutoSize = true;
            this.skinRadioButton2.BackColor = System.Drawing.Color.Transparent;
            this.skinRadioButton2.Checked = true;
            this.skinRadioButton2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinRadioButton2.DownBack = null;
            this.skinRadioButton2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinRadioButton2.Location = new System.Drawing.Point(261, 49);
            this.skinRadioButton2.MouseBack = null;
            this.skinRadioButton2.Name = "skinRadioButton2";
            this.skinRadioButton2.NormlBack = null;
            this.skinRadioButton2.SelectedDownBack = null;
            this.skinRadioButton2.SelectedMouseBack = null;
            this.skinRadioButton2.SelectedNormlBack = null;
            this.skinRadioButton2.Size = new System.Drawing.Size(74, 21);
            this.skinRadioButton2.TabIndex = 136;
            this.skinRadioButton2.TabStop = true;
            this.skinRadioButton2.Text = "退出程序";
            this.skinRadioButton2.UseVisualStyleBackColor = false;
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(23, 113);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(80, 17);
            this.skinLabel3.TabIndex = 135;
            this.skinLabel3.Text = "麦克风选择：";
            // 
            // skinComboBox_camera
            // 
            this.skinComboBox_camera.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.skinComboBox_camera.FormattingEnabled = true;
            this.skinComboBox_camera.Location = new System.Drawing.Point(109, 82);
            this.skinComboBox_camera.Name = "skinComboBox_camera";
            this.skinComboBox_camera.Size = new System.Drawing.Size(226, 22);
            this.skinComboBox_camera.TabIndex = 137;
            this.skinComboBox_camera.WaterText = "";
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(23, 82);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(80, 17);
            this.skinLabel1.TabIndex = 135;
            this.skinLabel1.Text = "摄像头选择：";
            // 
            // skinComboBox_mic
            // 
            this.skinComboBox_mic.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.skinComboBox_mic.FormattingEnabled = true;
            this.skinComboBox_mic.Location = new System.Drawing.Point(109, 113);
            this.skinComboBox_mic.Name = "skinComboBox_mic";
            this.skinComboBox_mic.Size = new System.Drawing.Size(226, 22);
            this.skinComboBox_mic.TabIndex = 137;
            this.skinComboBox_mic.WaterText = "";
            // 
            // skinCheckBox_autoRun
            // 
            this.skinCheckBox_autoRun.AutoSize = true;
            this.skinCheckBox_autoRun.BackColor = System.Drawing.Color.Transparent;
            this.skinCheckBox_autoRun.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinCheckBox_autoRun.DownBack = null;
            this.skinCheckBox_autoRun.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinCheckBox_autoRun.Location = new System.Drawing.Point(109, 151);
            this.skinCheckBox_autoRun.MouseBack = null;
            this.skinCheckBox_autoRun.Name = "skinCheckBox_autoRun";
            this.skinCheckBox_autoRun.NormlBack = null;
            this.skinCheckBox_autoRun.SelectedDownBack = null;
            this.skinCheckBox_autoRun.SelectedMouseBack = null;
            this.skinCheckBox_autoRun.SelectedNormlBack = null;
            this.skinCheckBox_autoRun.Size = new System.Drawing.Size(157, 21);
            this.skinCheckBox_autoRun.TabIndex = 138;
            this.skinCheckBox_autoRun.Text = "开机时自动启动GG2013";
            this.skinCheckBox_autoRun.UseVisualStyleBackColor = false;
            this.skinCheckBox_autoRun.CheckedChanged += new System.EventHandler(this.skinCheckBox1_CheckedChanged);
            // 
            // SystemSettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Back = ((System.Drawing.Image)(resources.GetObject("$this.Back")));
            this.BackLayout = false;
            this.BorderPalace = ((System.Drawing.Image)(resources.GetObject("$this.BorderPalace")));
            this.CanResize = false;
            this.CaptionFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.ClientSize = new System.Drawing.Size(354, 224);
            this.CloseBoxSize = new System.Drawing.Size(39, 20);
            this.CloseDownBack = global::GG.Properties.Resources.btn_close_down;
            this.CloseMouseBack = global::GG.Properties.Resources.btn_close_highlight;
            this.CloseNormlBack = global::GG.Properties.Resources.btn_close_disable;
            this.ControlBoxOffset = new System.Drawing.Point(0, -1);
            this.Controls.Add(this.skinCheckBox_autoRun);
            this.Controls.Add(this.skinComboBox_mic);
            this.Controls.Add(this.skinComboBox_camera);
            this.Controls.Add(this.skinRadioButton2);
            this.Controls.Add(this.skinRadioButton_hide);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.skinLabel3);
            this.Controls.Add(this.skinLabel2);
            this.Controls.Add(this.btnClose);
            this.DropBack = false;
            this.EffectWidth = 2;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaxDownBack = global::GG.Properties.Resources.btn_max_down;
            this.MaximizeBox = false;
            this.MaxMouseBack = global::GG.Properties.Resources.btn_max_highlight;
            this.MaxNormlBack = global::GG.Properties.Resources.btn_max_normal;
            this.MaxSize = new System.Drawing.Size(28, 20);
            this.MiniDownBack = global::GG.Properties.Resources.btn_mini_down;
            this.MinimizeBox = false;
            this.MiniMouseBack = global::GG.Properties.Resources.btn_mini_highlight;
            this.MiniNormlBack = global::GG.Properties.Resources.btn_mini_normal;
            this.MiniSize = new System.Drawing.Size(28, 20);
            this.Name = "SystemSettingForm";
            this.RestoreDownBack = global::GG.Properties.Resources.btn_restore_down;
            this.RestoreMouseBack = global::GG.Properties.Resources.btn_restore_highlight;
            this.RestoreNormlBack = global::GG.Properties.Resources.btn_restore_normal;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "系统设置";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinButton btnClose;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinRadioButton skinRadioButton_hide;
        private CCWin.SkinControl.SkinRadioButton skinRadioButton2;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinComboBox skinComboBox_camera;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinComboBox skinComboBox_mic;
        private CCWin.SkinControl.SkinCheckBox skinCheckBox_autoRun;
    }
}