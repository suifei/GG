﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CCWin;
using CCWin.Win32;
using CCWin.Win32.Const;
using System.Diagnostics;
using GG.Core;
using System.Configuration;
using ESPlus.Rapid;
using OMCS.Tools;
using OMCS.Passive;
using Microsoft.Win32;

namespace GG
{
    public partial class SystemSettingForm : CCSkinMain
    {
        public SystemSettingForm()
        {
            InitializeComponent();
            this.skinRadioButton_hide.Checked = !SystemSettings.Singleton.ExitWhenCloseMainForm;
            this.skinCheckBox_autoRun.Checked = SystemSettings.Singleton.AutoRun;

            List<CameraInformation> cameraList = OMCS.Tools.Camera.GetCameras();
            this.skinComboBox_camera.DataSource = cameraList;
            if (SystemSettings.Singleton.WebcamIndex < cameraList.Count)
            {
                this.skinComboBox_camera.SelectedIndex = SystemSettings.Singleton.WebcamIndex;
            }
            else
            {
                SystemSettings.Singleton.WebcamIndex = 0;               
                if (cameraList.Count > 0)
                {
                    this.skinComboBox_camera.SelectedIndex = 0;
                }
            }

            List<MicrophoneInformation> micList = OMCS.Tools.SoundDevice.GetMicrophones();
            this.skinComboBox_mic.DataSource = micList;
            if (SystemSettings.Singleton.MicrophoneIndex < micList.Count)
            {
                this.skinComboBox_mic.SelectedIndex = SystemSettings.Singleton.MicrophoneIndex;
            }
            else
            {
                SystemSettings.Singleton.MicrophoneIndex = 0;               
                if (micList.Count > 0)
                {
                    this.skinComboBox_mic.SelectedIndex = 0;
                }
            }

            SystemSettings.Singleton.Save();
        }        

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                SystemSettings.Singleton.AutoRun = this.skinCheckBox_autoRun.Checked;
                SystemSettings.Singleton.WebcamIndex = this.skinComboBox_camera.SelectedIndex;
                SystemSettings.Singleton.MicrophoneIndex = this.skinComboBox_mic.SelectedIndex;
                SystemSettings.Singleton.ExitWhenCloseMainForm = !this.skinRadioButton_hide.Checked;

                MultimediaManagerFactory.GetSingleton().CameraDeviceIndex = SystemSettings.Singleton.WebcamIndex;               
                SystemSettings.Singleton.Save();
                this.Close();
            }
            catch(Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void skinCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            //操作注册表，需要使用管理员身份启动程序。
            //GGHelper.RunWhenStart(this.skinCheckBox_autoRun.Checked, "GG2013", Application.ExecutablePath);
        }

        
         
    }
}
