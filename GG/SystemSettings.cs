﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using GG.Core;

namespace GG
{
    /// <summary>
    /// 系统设置。保存用户设置的数据。
    /// </summary>
    [Serializable]
    public class SystemSettings
    {
        private static string SystemSettingsFileName = "GGConfig.xml";

        private static SystemSettings singleton;
        /// <summary>
        /// 单例模式。
        /// </summary>
        public static SystemSettings Singleton
        {
            get
            {
                if (SystemSettings.singleton == null)
                {
                    SystemSettings.singleton = SystemSettings.Load();
                    if (SystemSettings.singleton == null)
                    {
                        SystemSettings.singleton = new SystemSettings();
                    }
                }

                return SystemSettings.singleton;
            }           
        }

        private SystemSettings() { }

        #region AutoRun
        private bool autoRun = false;
        /// <summary>
        /// 是否开机自动 运行
        /// </summary>
        public bool AutoRun
        {
            get { return autoRun; }
            set { autoRun = value; }
        }
        #endregion

        #region WebcamIndex
        private int webcamIndex = 0;
        /// <summary>
        /// 摄像头索引
        /// </summary>
        public int WebcamIndex
        {
            get { return webcamIndex; }
            set { webcamIndex = value; }
        }
        #endregion

        #region MicrophoneIndex
        private int microphoneIndex = 0;
        /// <summary>
        /// 麦克风索引。
        /// </summary>
        public int MicrophoneIndex
        {
            get { return microphoneIndex; }
            set { microphoneIndex = value; }
        } 
        #endregion

        #region ExitWhenCloseMainForm
        private bool exitWhenCloseMainForm = false;
        /// <summary>
        /// 点击关闭按钮的时候，是否退出程序。
        /// </summary>
        public bool ExitWhenCloseMainForm
        {
            get { return exitWhenCloseMainForm; }
            set { exitWhenCloseMainForm = value; }
        }
        #endregion

        public void Save()
        {
            string xml = GGHelper.XmlObject(this);
            string path = AppDomain.CurrentDomain.BaseDirectory + SystemSettings.SystemSettingsFileName;
            GGHelper.GenerateFile(path, xml);
        }

        private static SystemSettings Load()
        {
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + SystemSettings.SystemSettingsFileName;
                if (!File.Exists(path))
                {
                    return null;
                }

                string xml = GGHelper.GetFileContent(path);
                return GGHelper.ObjectXml<SystemSettings>(xml);
            }
            catch
            {
                return null;
            }

        }
    }
}
