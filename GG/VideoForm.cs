﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CCWin;
using CCWin.Win32;
using CCWin.Win32.Const;
using System.Diagnostics;
using ESBasic;
using OMCS.Passive;

namespace GG
{
    public partial class VideoForm : CCSkinMain
    {
        private bool activeHungUp = true;
        private IMultimediaManager multimediaManager;

        /// <summary>
        /// 当自己挂断视频时，触发此事件。
        /// </summary>
        public event CbGeneric ActiveHungUpVideo;

        private string currentUserID;
        private string friendID;
        public VideoForm(IMultimediaManager mgr ,string currentID, string _friendID, string friendName, bool waitingAnswer)
        {
            InitializeComponent();
            this.multimediaManager = mgr;
            this.currentUserID = currentID;
            this.friendID = _friendID;
            this.Text = string.Format("正在和{0}视频会话" ,friendName);

            if (!waitingAnswer) //同意视频，开始连接
            {                
                this.OnAgree();
            }
        }

        /// <summary>
        /// 对方同意视频会话
        /// </summary>
        public void OnAgree()
        {
            this.toolStripLabel_msg.Text = "正在连接 . . .";
            this.dynamicCameraConnector1.SetViewer(this.skinPanel1);
            this.dynamicCameraConnector1.BeginConnect(this.friendID);
            this.microphoneConnector1.BeginConnect(this.friendID);
            this.cameraConnector1.BeginConnect(this.currentUserID);            
        }

        /// <summary>
        /// 对方挂断视频
        /// </summary>
        public void OnHungUpVideo()
        {
            this.activeHungUp = false;
            this.Close();
        }


        private int totalSeconds = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            ++this.totalSeconds;
            this.ShowVideoTime();
        }

        //显示已经会话的时间
        private void ShowVideoTime()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new CbGeneric(this.ShowVideoTime));
            }
            else
            {
                int min = this.totalSeconds / 60;
                int sec = this.totalSeconds % 60;
                this.toolStripLabel_time.Text = string.Format("{0}:{1}  " ,min.ToString("00") ,sec.ToString("00"));
            }
        }

        //自己挂断视频
        private void toolStripButton1_Click(object sender, EventArgs e)
        {            
            this.Close();
        }

        private void VideoForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.cameraConnector1.Disconnect();
            this.dynamicCameraConnector1.Disconnect();
            this.microphoneConnector1.Disconnect();
            this.timer1.Stop();

            if (this.activeHungUp)
            {
                if (this.ActiveHungUpVideo != null)
                {
                    this.ActiveHungUpVideo();
                }
            }
        }

        private void dynamicCameraConnector1_ConnectEnded(ConnectResult res)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new CbGeneric<ConnectResult>(this.dynamicCameraConnector1_ConnectEnded), res);
            }
            else
            {
                this.toolStripLabel_msg.Visible = false;
                this.toolStripLabel_netstate.Visible = true;
                this.toolStripButton_hideMyself.Visible = true;
                this.toolStripButton_camera.Visible = true;
                this.toolStripButton_microphone.Visible = true;
                this.toolStripButton_speaker.Visible = true;
                this.toolStripSeparator1.Visible = true;
                this.timer1.Start();
            }
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            this.skinPanel_Myself.Visible = !this.skinPanel_Myself.Visible;
            this.toolStripButton_hideMyself.Text =this.skinPanel_Myself.Visible ? "隐藏小窗口" :"显示小窗口";
        }

        private void toolStripButton1_Click_2(object sender, EventArgs e)
        {
            this.multimediaManager.OutputVideo = !this.multimediaManager.OutputVideo;
            this.toolStripButton_camera.Text = this.multimediaManager.OutputVideo ? "状态：正常输出视频" : "状态：不输出视频";
            this.toolStripButton_camera.Image = this.multimediaManager.OutputVideo ? this.imageList1.Images[0] : this.imageList1.Images[1]; 
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            this.microphoneConnector1.Mute = !this.microphoneConnector1.Mute;
            this.toolStripButton_speaker.Text = this.microphoneConnector1.Mute ? "状态：静音" : "状态：正常";
            this.toolStripButton_speaker.Image = this.microphoneConnector1.Mute ? this.imageList1.Images[3] : this.imageList1.Images[2]; 
        }

        private void toolStripButton_microphone_Click(object sender, EventArgs e)
        {
            this.multimediaManager.OutputAudio = !this.multimediaManager.OutputAudio;
            this.toolStripButton_microphone.Text = this.multimediaManager.OutputAudio ? "状态：正常输出音频" : "状态：不输出音频";
            this.toolStripButton_microphone.Image = this.multimediaManager.OutputAudio ? this.imageList1.Images[4] : this.imageList1.Images[5]; 
        }
    }
}
