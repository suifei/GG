using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ESBasic;
using ESPlus.FileTransceiver;
using ESPlus.Application.FileTransfering.Passive;
using ESBasic.Threading.Engines;

namespace GGLib
{
    /// <summary>
    /// 文件传送查看器。用于查看与某个好友的所有文件传送的进度状态信息。
    /// 注意，FileTransferingViewer的所有事件都是在UI线程中触发的。
    /// </summary>
    public partial class FileTransferingViewer : UserControl, IFileTransferingViewer ,IEngineActor
    {
        private ESBasic.Func<TransferingProject, bool> projectFilter;
        private IFileOutter fileOutter;             
        private string friendID;
        private AgileCycleEngine agileCycleEngine;

        #region event FileTransferingViewer的所有事件都是在UI线程中触发的。

        /// <summary>
        /// 当某个文件开始续传时，触发该事件。参数为FileName - isSending
        /// </summary>
        public event CbGeneric<string, bool> FileResumedTransStarted; 

        /// <summary>
        /// 当某个文件传送完毕时，触发该事件。参数为FileName - isSending - comment
        /// </summary>
        public event CbGeneric<string, bool, string> FileTransCompleted;

        /// <summary>
        /// 当某个文件传送中断时，触发该事件。参数为FileName - isSending - FileTransDisrupttedType
        /// </summary>
        public event CbGeneric<string, bool, FileTransDisrupttedType> FileTransDisruptted;

        /// <summary>
        /// 当某个文件传送开始时，触发该事件。参数为FileName - isSending
        /// </summary>
        public event CbGeneric<string, bool> FileTransStarted;

        /// <summary>
        /// 当所有文件都传送完成时，触发该事件。
        /// </summary>
        public event CbSimple AllTaskFinished;       
        #endregion       

        #region Ctor
        public FileTransferingViewer()
        {
            InitializeComponent();

            this.agileCycleEngine = new AgileCycleEngine(this);
            this.agileCycleEngine.DetectSpanInSecs = 1;
            this.agileCycleEngine.Start();

            this.FileResumedTransStarted += delegate { };
            this.FileTransCompleted += delegate { };
            this.FileTransDisruptted += delegate { };
            this.FileTransStarted += delegate { };
            this.AllTaskFinished += delegate { };            
        } 
        #endregion

        #region Initialize
        /// <summary>
        /// 初始化控件。
        /// </summary>
        /// <param name="friendUserID">要关注与哪个好友的文件传送状态。如果传入null，将显示所有正在传送的文件的状态</param>  
        /// <param name="filter">关注项目进一步过滤</param>        
        public void Initialize(string friendUserID, IFileOutter _fileOutter, ESBasic.Func<TransferingProject, bool> filter)
        {
            this.friendID = friendUserID;
            this.fileOutter = _fileOutter;
            this.projectFilter = filter;
            

            //this.fileOutter.FileRequestReceived += new ESPlus.Application.FileTransfering.CbFileRequestReceived(fileOutter_FileRequestReceived);
            this.fileOutter.FileSendingEvents.FileTransStarted += new CbGeneric<TransferingProject>(fileSenderManager_FileTransStarted);
            this.fileOutter.FileSendingEvents.FileTransCompleted += new CbGeneric<TransferingProject>(fileSenderManager_FileTransCompleted);
            this.fileOutter.FileSendingEvents.FileTransDisruptted += new CbGeneric<TransferingProject, FileTransDisrupttedType>(fileSenderManager_FileTransDisruptted);
            this.fileOutter.FileSendingEvents.FileTransProgress += new CbFileSendedProgress(fileSenderManager_FileTransProgress);
            this.fileOutter.FileSendingEvents.FileResumedTransStarted += new CbGeneric<TransferingProject>(fileSenderManager_FileResumedTransStarted);

            this.fileOutter.FileReceivingEvents.FileTransStarted += new CbGeneric<TransferingProject>(fileReceiverManager_FileTransStarted);
            this.fileOutter.FileReceivingEvents.FileResumedTransStarted += new CbGeneric<TransferingProject>(fileReceiverManager_FileResumedTransStarted);
            this.fileOutter.FileReceivingEvents.FileTransCompleted += new CbGeneric<TransferingProject>(fileReceiverManager_FileTransCompleted);
            this.fileOutter.FileReceivingEvents.FileTransDisruptted += new CbGeneric<TransferingProject, FileTransDisrupttedType>(fileReceiverManager_FileTransDisruptted);
            this.fileOutter.FileReceivingEvents.FileTransProgress += new CbFileSendedProgress(fileReceiverManager_FileTransProgress);            
        }      

        /// <summary>
        /// 当收到 发送文件的请求时    显示fileTransferItem
        /// </summary>        
        public void NewFileTransferItem(string projectID, bool offlineFile)
        {
            TransferingProject pro = this.fileOutter.GetTransferingProject(projectID);
            if (pro == null)
            {
                return;
            }

            this.AddFileTransItem(pro ,offlineFile);
        }

        /// <summary>
        ///  一定要在控件销毁的时候，取消预订的事件。否则，已被释放的控件的处理函数仍然会被调用，而引发"对象已经被释放"的异常。
        /// </summary>        
        public void FileTransferingViewer_Disposed()//object sender, EventArgs e)
        {
            this.agileCycleEngine.StopAsyn();
           
            this.fileOutter.FileSendingEvents.FileTransStarted -= new CbGeneric<TransferingProject>(fileSenderManager_FileTransStarted);
            
            this.fileOutter.FileSendingEvents.FileTransCompleted -= new CbGeneric<TransferingProject>(fileSenderManager_FileTransCompleted);
            this.fileOutter.FileSendingEvents.FileTransDisruptted -= new CbGeneric<TransferingProject, FileTransDisrupttedType>(fileSenderManager_FileTransDisruptted);
            this.fileOutter.FileSendingEvents.FileTransProgress -= new CbFileSendedProgress(fileSenderManager_FileTransProgress);
            this.fileOutter.FileSendingEvents.FileResumedTransStarted -= new CbGeneric<TransferingProject>(fileSenderManager_FileResumedTransStarted);
            
            this.fileOutter.FileReceivingEvents.FileTransStarted -= new CbGeneric<TransferingProject>(fileReceiverManager_FileTransStarted);
            this.fileOutter.FileReceivingEvents.FileResumedTransStarted -= new CbGeneric<TransferingProject>(fileReceiverManager_FileResumedTransStarted);
            this.fileOutter.FileReceivingEvents.FileTransCompleted -= new CbGeneric<TransferingProject>(fileReceiverManager_FileTransCompleted);
            this.fileOutter.FileReceivingEvents.FileTransDisruptted -= new CbGeneric<TransferingProject, FileTransDisrupttedType>(fileReceiverManager_FileTransDisruptted);
            this.fileOutter.FileReceivingEvents.FileTransProgress -= new CbFileSendedProgress(fileReceiverManager_FileTransProgress);
        }   

        void fileReceiverManager_FileResumedTransStarted(TransferingProject info)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CbGeneric<TransferingProject>(this.fileReceiverManager_FileResumedTransStarted), info);
            }
            else
            {
                if ((this.friendID != null) && (info.DestUserID != this.friendID)) //此时，还不存在对应的Item
                {
                    return;
                }
                FileTransferItem item = this.GetFileTransItem(info.ProjectID);
                if (item != null)
                {
                    this.FileResumedTransStarted(info.ProjectName, false);
                }
            }
        }

        void fileSenderManager_FileResumedTransStarted(TransferingProject info)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CbGeneric<TransferingProject>(this.fileSenderManager_FileResumedTransStarted), info);
            }
            else
            {
                this.AddFileTransItem(info);
                FileTransferItem item = this.GetFileTransItem(info.ProjectID);
                if (item != null)
                {
                    this.FileResumedTransStarted(info.ProjectName ,true);
                }
            }
        }

        /// <summary>
        /// 当前是否有文件正在传送中。
        /// </summary>   
        public bool IsFileTransfering
        {
            get
            {
                return (this.flowLayoutPanel1.Controls.Count > 0);
            }
        }
        #endregion     

        #region fileReceiverManager
        #region fileReceiverManager_FileTransStarted
        /// <summary>
        /// 文件传输开始的 时候， 接收方  触发
        /// </summary>
        /// <param name="transInfo"></param>
        void fileReceiverManager_FileTransStarted(TransferingProject transInfo)
        {
            this.AddFileTransItem(transInfo);
            FileTransferItem item = this.GetFileTransItem(transInfo.ProjectID);
            if (item != null)
            {
                item.IsTransfering = true;                
            }
        } 
        #endregion
        #region fileReceiverManager_FileTransProgress
        /// <summary>
        /// 文件传输中，接收方 触发
        /// </summary>        
        void fileReceiverManager_FileTransProgress(string projectID, ulong total, ulong sended)
        {
            FileTransferItem item = this.GetFileTransItem(projectID);
            if (item != null)
            {
                item.SetProgress(total, sended);
            }
        } 
        #endregion
        #region fileReceiverManager_FileTransDisruptted
        void fileReceiverManager_FileTransDisruptted(TransferingProject info, FileTransDisrupttedType disrupttedType)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CbGeneric<TransferingProject, FileTransDisrupttedType>(this.fileReceiverManager_FileTransDisruptted), info, disrupttedType);
            }
            else
            {               
                FileTransferItem item = this.GetFileTransItem(info.ProjectID);
                if (item != null)
                {
                    this.flowLayoutPanel1.Controls.Remove(item);
                    this.FileTransDisruptted(info.ProjectName, info.IsSender, disrupttedType);
                }
            }
        } 
        #endregion
        #region fileReceiverManager_FileTransCompleted
        void fileReceiverManager_FileTransCompleted(TransferingProject info)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CbGeneric<TransferingProject>(this.fileReceiverManager_FileTransCompleted), info);
            }
            else
            {              
                FileTransferItem item = this.GetFileTransItem(info.ProjectID);
                if (item != null)
                {
                    this.flowLayoutPanel1.Controls.Remove(item);
                    this.FileTransCompleted(info.ProjectName, info.IsSender, info.Comment);
                }
            }
        } 
        #endregion
        #endregion        

        #region fileSenderManager
        #region fileSenderManager_FileTransStarted
        /// <summary>
        /// 文件传输开始 的时候，发送方触发
        /// </summary>
        /// <param name="transInfo"></param>
        void fileSenderManager_FileTransStarted(TransferingProject transInfo)
        {
            this.AddFileTransItem(transInfo);
            FileTransferItem item = this.GetFileTransItem(transInfo.ProjectID);
            if (item != null)
            {
                item.IsTransfering = true;                
            }
        } 
        #endregion
        #region fileSenderManager_FileTransProgress
        void fileSenderManager_FileTransProgress(string projectID, ulong total, ulong sended)
        {
            FileTransferItem item = this.GetFileTransItem(projectID);
            if (item != null)
            {
                item.SetProgress(total, sended);
            }
        } 
        #endregion
        #region fileSenderManager_FileTransDisruptted
        void fileSenderManager_FileTransDisruptted(TransferingProject info, FileTransDisrupttedType disrupttedType)
        {
            if (this.IsDisposed)
            {
                return;
            }
            if (this.InvokeRequired)
            {
                this.Invoke(new CbGeneric<TransferingProject, FileTransDisrupttedType>(this.fileSenderManager_FileTransDisruptted), info, disrupttedType);
            }
            else
            {
                FileTransferItem item = this.GetFileTransItem(info.ProjectID);
                if (item != null)
                {
                    this.flowLayoutPanel1.Controls.Remove(item);
                    this.FileTransDisruptted(info.ProjectName, info.IsSender, disrupttedType);
                }
            }
        } 
        #endregion
        #region fileSenderManager_FileTransCompleted
        void fileSenderManager_FileTransCompleted(TransferingProject info)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new CbGeneric<TransferingProject>(this.fileSenderManager_FileTransCompleted), info);
            }
            else
            {               
                FileTransferItem item = this.GetFileTransItem(info.ProjectID);
                if (item != null)
                {
                    this.flowLayoutPanel1.Controls.Remove(item);
                    this.FileTransCompleted(info.ProjectName, info.IsSender ,info.Comment);
                }
            }
        } 
        #endregion
        #endregion

        #region AddFileTransItem ,GetFileTransItem
        private void AddFileTransItem(TransferingProject project)
        {
            this.AddFileTransItem(project, false);
        }
        private void AddFileTransItem(TransferingProject project, bool offlineFile)
        {
            if (this.projectFilter != null)
            {
                if (!this.projectFilter(project))
                {
                    return;
                }
            }

            if (this.InvokeRequired)
            {
                this.Invoke(new CbGeneric<TransferingProject, bool>(this.AddFileTransItem), project, offlineFile);
            }
            else
            {
                FileTransferItem fileTransItem = this.GetFileTransItem(project.ProjectID);
                if (fileTransItem != null)
                {
                    return;
                }
                fileTransItem = new FileTransferItem();
                fileTransItem.FileCanceled += new CbFileCanceled(fileTransItem_FileCanceled);
                fileTransItem.FileReceived += new CbFileReceived(fileTransItem_FileReceived);
                fileTransItem.FileRejected += new CbFileRejected(fileTransItem_FileRejected);
                fileTransItem.Initialize(project, offlineFile);
                this.flowLayoutPanel1.Controls.Add(fileTransItem);
                this.FileTransStarted(project.ProjectName, project.IsSender);
            }
        }

        private FileTransferItem GetFileTransItem(string projectID)
        {
            foreach (FileTransferItem item in this.flowLayoutPanel1.Controls)
            {
                if (item.TransmittingProject.ProjectID == projectID)
                {
                    return item;
                }
            }      

            return null; 
        }        

        void fileTransItem_FileRejected(string projectID)
        {
            this.fileOutter.RejectFile(projectID);            
        }

        void fileTransItem_FileReceived(FileTransferItem item, string projectID, bool isSend, string savePath)
        {
            this.fileOutter.BeginReceiveFile(projectID, savePath);
        }        

        void fileTransItem_FileCanceled(FileTransferItem item, string projectID, bool isSend)
        {
            this.fileOutter.CancelTransfering(projectID);             
        }
        #endregion
      

        #region flowLayoutPanel1_ControlRemoved
        private void flowLayoutPanel1_ControlRemoved(object sender, ControlEventArgs e)
        {
            if (this.flowLayoutPanel1.Controls.Count == 0)
            {
                this.AllTaskFinished();
            }
        } 
        #endregion

        #region EngineAction
        public bool EngineAction()
        {
            try
            {
                foreach (object obj in this.flowLayoutPanel1.Controls)
                {
                    FileTransferItem item = obj as FileTransferItem;
                    if (item != null && item.IsTransfering)
                    {
                        item.CheckZeroSpeed();
                    }
                }
            }
            catch { }
            return true;
        } 
        #endregion
    }

    internal delegate FileTransferItem CbGetFileTransItem(string projectID, bool isSended);
    internal delegate FileTransferItem CbGetFileTransItem2(string projectID);
    public delegate void CbFileReceiveCanceled(string friend ,string projectID) ;
    public delegate void CbCancelFile(string projectID,bool isSsend);
}
